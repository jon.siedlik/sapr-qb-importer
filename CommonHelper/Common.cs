﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CommonHelper
{
    public class Common
    {
        public static List<string> PropertiesFromType(object atype)
        {
            if (atype == null)
                return new List<string> { };
            Type t = atype.GetType();
            PropertyInfo[] props = t.GetProperties();
            List<string> propNames = new List<string>();
            foreach (PropertyInfo prp in props)
            {
                propNames.Add(prp.Name);
            }
            return propNames.ToList();
        }

        public static string GetDuration(decimal total)
        {
            string duration = "";
            string Total = total.ToString("F2");
            decimal hoursTotal = 0;
            decimal minsTotal = 0;
            string hoursPart = Total.Substring(0, Total.IndexOf("."));
            hoursTotal = hoursTotal + Convert.ToDecimal(hoursPart);
            string minsPart = Total.Substring(Total.IndexOf(".") + 1);
            if (Convert.ToDecimal(minsPart) > 0)
            {
                minsTotal = Convert.ToInt32(Convert.ToDecimal(minsPart) * 60 / 100);
            }

            if (total > 0)
            {
                duration = "PT" + hoursTotal + "H" + minsTotal + "M0S";
            }
            else
            {
                duration = "PT0H0M0S";
            }
            return duration;
        }

        public static string GetTimeFromDuration(string time)
        {
            time = time.TrimStart('P', 'T').TrimEnd('0', 'S');
            string[] st = time.TrimEnd('M').Split('H');
            int hr = Convert.ToInt32(st[0]);
            int mins = Convert.ToInt32(st[1]);
            if (mins > 0)
            {
                mins = mins * 100 / 60;
            }
            return hr.ToString() + "." + mins;
        }

        public static List<T> DataTableToList<T>(DataTable table) where T : new()
        {
            List<T> list = new List<T>();
            var typeProperties = typeof(T).GetProperties().Select(propertyInfo => new
            {
                PropertyInfo = propertyInfo,
                Type = Nullable.GetUnderlyingType(propertyInfo.PropertyType) ?? propertyInfo.PropertyType
            }).ToList();

            foreach (var row in table.Rows.Cast<DataRow>())
            {
                T obj = new T();
                foreach (var typeProperty in typeProperties)
                {
                    if (table.Columns.Contains(typeProperty.PropertyInfo.Name))
                    {

                        object value = row[typeProperty.PropertyInfo.Name];
                        object safeValue = value == null || DBNull.Value.Equals(value)
                            ? null
                            : Convert.ChangeType(value, typeProperty.Type);

                        typeProperty.PropertyInfo.SetValue(obj, safeValue, null);
                    }
                }
                list.Add(obj);
            }
            return list;
        }

        public static string GetCommonBaseDirectory()
        {
            var versionInfo = FileVersionInfo.GetVersionInfo(Assembly.GetEntryAssembly().Location);
            string baseDir = $"{Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData)}\\{versionInfo.CompanyName}";
            if (!Directory.Exists(baseDir))
            {
                Directory.CreateDirectory(baseDir);
            }
            return baseDir;

        }

        public static List<string> GetPropertyValues<T>(T user)
        {
            Type type = user.GetType();
            PropertyInfo[] props = type.GetProperties();
            List<string> str = new List<string>();
            foreach (var prop in props)
            {
                str.Add(prop.Name);
            }
            return str;
        }
    }
}
