﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonHelper.Models
{
    public class TimeTracking
    {
        [DisplayName("Name")]
        public string Name { get; set; }

        [DisplayName("PayrollID")]
        public string PayrollID { get; set; }

        [DisplayName("AccountNumber")]
        public string AccountNumber { get; set; }

        [DisplayName("Bilable Status")]
        public string BilableStatus { get; set; }
        
        [DisplayName("Class Name")]
        public string ClassName { get; set; }
        
        [DisplayName("Customer")]
        public string Customer { get; set; }
        
        [DisplayName("Duration")]
        public string Duration { get; set; }
        
        [DisplayName("Notes")]
        public string Notes { get; set; }
        
        [DisplayName("PayrollItemName")]
        public string PayrollItemName { get; set; }
        
        [DisplayName("Service Item Name")]
        public string ServiceItemName { get; set; }
        
        [DisplayName("Time Tracking Date")]
        public string TimeTrackingDate { get; set; }

        [DisplayName("Billable")]
        public string IsBillable { get; set; }
    }
}
