﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonHelper.Models
{
    public class ImportFromExcelField
    {
        public string FieldName { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string SpreadsheetFieldType { get; set; }
        public int RealFieldType { get; set; }
        public bool? IsRequired { get; set; }
    }
}
