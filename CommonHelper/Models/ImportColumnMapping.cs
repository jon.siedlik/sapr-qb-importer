﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonHelper.Models
{
    public class ImportColumnMapping
    {
        public string UserFieldName { get; set; }
        public string QuickbookFieldName { get; set; }
        public string DefaultValue { get; set; }
    }
}
