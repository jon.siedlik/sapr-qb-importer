﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonHelper.Models
{
    public class CustomerJob
    {
        [DisplayName("Name")]
        public string Name { get; set; }

        [DisplayName("Customer")]
        public string ParentRef { get; set; }
    }
}
