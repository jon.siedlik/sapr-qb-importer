﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CommonHelper
{
    public static class EnumHelper
    {
        public static IEnumerable<EnumDropDown> ToSelectList(this Enum enumValue)
        {
            return from Enum e in Enum.GetValues(enumValue.GetType())
                   select new EnumDropDown
                   {
                       Text = e.ToDescription(),
                       Value = Convert.ToInt32(e)
                   };
        }

        public static string ToDescription(this Enum value)
        {
            var attributes = (DescriptionAttribute[])value.GetType().GetField(value.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : value.ToString();
        }
    }

    public class EnumDropDown
    {
        public int Value { get; set; }

        public string Text { get; set; }
    }
}
