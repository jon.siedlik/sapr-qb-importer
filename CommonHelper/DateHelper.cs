﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonHelper
{
    public class DateHelper
    {
        public static string ToDateString(string dateTimeString)
        {
            if (string.IsNullOrEmpty(dateTimeString))
            {
                return null;
            }

            DateTime dateTime = ParseDateTime(dateTimeString);
            return dateTime.ToString("yyyy-MM-dd");
        }

        public static DateTime ParseDateTime(string s)
        {
            var culture = System.Globalization.CultureInfo.CurrentCulture;
            return DateTime.ParseExact(s, "yyyy-MM-dd HH:mm:ss", culture);
        }

        public static string ToDateString(DateTime? dateTime)
        {
            if(!dateTime.HasValue)
            {
                return string.Empty;
            }

            return dateTime.Value.ToString("yyyy-MM-dd");
        }
    }
}
