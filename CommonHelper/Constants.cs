﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonHelper
{
    public class Constants
    {
        public const string cAPP_NAME = "Business Importer";
        public const string cOWNER_FIELDS = "{16A30407-3177-40C4-B351-901245651981}";
        public const string cQBCountry = "US";
        public const string qbxmlVersion = "13.0";
        public static short MINOR_VERSION
        {
            get
            {
                return 0;
            }
        }
        public static string MAPPING_DIRECTORY = StringHelper.RemoveWhiteSpace("Mapping");
        public static string PATH_LOACATION = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

        public static string SettingFiles = Path.Combine(Common.GetCommonBaseDirectory(), "GeneralSetting.xml");

        #region RequiredFields

        public static string[] RequiredFieldForTimeTracking = new string[] { "Name", "PayrollItemName", "TimeTrackingDate", "Duration" };
        public static string[] RequiredFieldForJob = new string[] { "Name", "ParentRef" };

        #endregion


        #region Mapping Files

        public const string TIMESHEET_MAPFILE = "timesheet-mapping.xml";
        public const string CUSTOMERJOB_MAPFILE = "job-mapping.xml";

        #endregion

    }
}
