﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonHelper
{
    public static class GlobalValue
    {
        public struct Reponse
        {
            public bool IsError;
            public string Message;
            public int RecordCount;
            public object EntityObject;
        }
    }
}
