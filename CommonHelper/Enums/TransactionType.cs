﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CommonHelper.Enums
{
    public enum TransactionType : int
    {
        [Description("Time Tracking")]
        TimeTracking = 1,

        [Description("Job")]
        Job = 2,
    }
}
