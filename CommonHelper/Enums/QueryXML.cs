﻿using CommonHelper;
using Logs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace CommonHelper.Enums
{
    public enum QueryXML
    {
        [Description(SyncConstant.QUERY_XML_PREFIX + "<CustomerQueryRq>\r\n<ActiveStatus >" + SyncConstant.RECORD_ACTIVE_STATUS + "</ActiveStatus>\r\n" + SyncConstant.PARAMETER_CAPTION + "<IncludeRetElement >ListID</IncludeRetElement><IncludeRetElement >AdditionalContactRef</IncludeRetElement></CustomerQueryRq>" + SyncConstant.QUERY_XML_POSTFIX)]
        Customer,

        [Description(SyncConstant.QUERY_XML_PREFIX + "<CustomerAddRq>\r\n" + SyncConstant.PARAMETER_CAPTION + "</CustomerAddRq>" + SyncConstant.QUERY_XML_POSTFIX)]
        CustomerAdd,

        [Description(SyncConstant.QUERY_XML_PREFIX + "<TimeTrackingAddRq>\r\n" + SyncConstant.PARAMETER_CAPTION + "</TimeTrackingAddRq>" + SyncConstant.QUERY_XML_POSTFIX)]
        TimeTracking,

        [Description(SyncConstant.QUERY_XML_PREFIX + "<TimeTrackingQueryRq>\r\n" + SyncConstant.PARAMETER_CAPTION + "</TimeTrackingQueryRq>" + SyncConstant.QUERY_XML_POSTFIX)]
        TimeTrackingQuery,

        [Description(SyncConstant.QUERY_XML_PREFIX + "<EmployeeQueryRq>\r\n" + SyncConstant.PARAMETER_CAPTION + "<ActiveStatus>All</ActiveStatus><OwnerID>0</OwnerID></EmployeeQueryRq >" + SyncConstant.QUERY_XML_POSTFIX)]
        EmployeeQuery,

        [Description(SyncConstant.QUERY_XML_PREFIX + "<EmployeeAddRq>\r\n" + SyncConstant.PARAMETER_CAPTION + "</EmployeeAddRq>" + SyncConstant.QUERY_XML_POSTFIX)]
        Employee,

    }

    public static class SyncConstant
    {
        internal const string QUERY_XML_PREFIX = "<?xml version=\"1.0\" ?>\r\n<?qbxml version=\""+ Constants.qbxmlVersion +"\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"stopOnError\">\r\n";
        public const string PARAMETER_CAPTION = "PARAMETER";
        public const int TOTAL_ENTITY_COUNT = 10;
        public const string QUERY_XML_POSTFIX = "\r\n</QBXMLMsgsRq>\r\n</QBXML>";
        public const string RECORD_ACTIVE_STATUS = "All";
        public static bool IsInsertingInQuickBooks { get; set; }

        public static string EnumDescription(this Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (attributes.Length > 0)
                return attributes[0].Description;
            return value.ToString();
        }
        public static string EnumName(this Enum value)
        {
            return value.ToString();
        }
        /// <summary>
        /// Serialize Xml to List
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xml"></param>
        /// <param name="query"></param>
        /// <param name="includeRootElement"></param>
        /// <returns></returns>
        public static List<T> SerializeXml<T>(string xml, QueryXML query, bool includeRootElement = true)
        {

            string entityName = query.EnumName();
            if (entityName.Contains("Query"))
                entityName = entityName.Replace("Query", "");

            XmlSerializer x;
            if (includeRootElement)
            {
                XmlRootAttribute xRoot = new XmlRootAttribute { ElementName = entityName + "QueryRs", IsNullable = true };
                x = new XmlSerializer(typeof(List<T>), xRoot);
            }
            else
            {
                x = new XmlSerializer(typeof(T));
            }


            try
            {
                object deserializedObject = x.Deserialize(new StringReader(xml));
                if (includeRootElement)
                {
                    List<T> ret = (List<T>)deserializedObject;
                    return ret;
                }
                else
                {
                    T ret = (T)deserializedObject;
                    List<T> t = new List<T> { ret };
                    return t;
                }
            }
            catch (Exception)
            {
                return new List<T>();
            }

        }

        /// <summary>
        /// Generate XML String
        /// </summary>
        /// <param name="xmlString"></param>
        /// <returns></returns>
        public static string GenerateXML(string xmlString)
        {
            string xml = "";

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xmlString);
            XmlNode selectSingleNode = xmlDoc.SelectSingleNode("QBXML");
            if (selectSingleNode != null)
            {
                XmlNode singleNode = selectSingleNode.SelectSingleNode("QBXMLMsgsRs");
                if (singleNode != null)
                    xml = singleNode.InnerXml;
            }

            return xml;
        }

    }
}
