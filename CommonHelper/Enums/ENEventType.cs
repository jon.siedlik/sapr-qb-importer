﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonHelper.Enums
{
	public enum ENEventType : byte
	{
		None,
		Data,
		UI,
		UIExtension
	}
}
