﻿using Aspose.Cells;
using Aspose.Cells.Tables;
using CommonHelper;
using CommonHelper.Enums;
using CommonHelper.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BusinessImporter.Helper
{
    public class ExcelReader
    {
        private string filePath;
        public ExcelReader(string filePath)
        {
            this.filePath = filePath;
        }

        public List<string> GetListOfSheet()
        {
            Workbook wb = new Workbook(this.filePath);
            return wb.Worksheets.Select(a => a.Name).ToList();
        }

        public List<string> GetListOfExcelColumn(string sheetName)
        {
            List<string> result = new List<string>();
            Workbook wb = new Workbook(this.filePath);
            Worksheet ws = wb.Worksheets[sheetName];
            ListObject tbl = ws.ListObjects[0];
            for (int i = 0; i < tbl.ListColumns.Count; i++)
            {
                ListColumn lc = tbl.ListColumns[i];
                result.Add(lc.Name);
            }

            return result;

        }

        public DataTable GetDataFromExcelFile(string sheetName)
        {
            Workbook wb = new Workbook(this.filePath);
            var sheet = wb.Worksheets[sheetName];
            var cells = sheet.Cells;

            ExportTableOptions opts = new ExportTableOptions();
            opts.CheckMixedValueType = true;
            opts.ExportColumnName = true;
            var table = cells.ExportDataTable(0, 0, cells.MaxDataRow, cells.MaxDataColumn + 1, opts);

            return table;

        }

        public List<ImportColumnMapping> GetColumnMappings(DataTable excelData, List<ImportFromExcelField> fields, int transactionType)
        {
            List<ImportColumnMapping> columnMappings = new List<ImportColumnMapping>();
            string mappingPath = string.Empty;
            DataSet ds = new DataSet();
            switch ((TransactionType)transactionType)
            {
                case TransactionType.TimeTracking:
                    mappingPath = Path.Combine(Common.GetCommonBaseDirectory(), Constants.MAPPING_DIRECTORY, Constants.TIMESHEET_MAPFILE);
                    columnMappings.AddRange(GetImportColumns(fields, excelData));
                    XMLFileMapping(mappingPath, ds, TransactionType.TimeTracking, columnMappings);
                    break;
                case TransactionType.Job:
                    mappingPath = Path.Combine(Common.GetCommonBaseDirectory(), Constants.MAPPING_DIRECTORY, Constants.CUSTOMERJOB_MAPFILE);
                    columnMappings.AddRange(GetImportColumns(fields, excelData));
                    XMLFileMapping(mappingPath, ds, TransactionType.Job, columnMappings);
                    break;
                default:
                    break;
            }
            return columnMappings;
        }

        private List<ImportColumnMapping> GetImportColumns(List<ImportFromExcelField> fields, DataTable excelData)
        {
            List<ImportColumnMapping> columnMappings = new List<ImportColumnMapping>();
            foreach (var field in fields)
            {
                var fieldName = field.FieldName;
                var fieldTitle = field.Title;
                var dtColumn = getUserColumnName(fieldTitle, excelData.Columns);

                var mapping = new ImportColumnMapping();
                mapping.QuickbookFieldName = fieldName;
                mapping.DefaultValue = "";
                mapping.UserFieldName = dtColumn?.ColumnName;
                columnMappings.Add(mapping);
            }

            return columnMappings;
        }

        private void XMLFileMapping(string mappingPath, DataSet dataSet, TransactionType transactionType, List<ImportColumnMapping> columnMappings)
        {
            switch (transactionType)
            {
                case TransactionType.TimeTracking:
                    if (File.Exists(mappingPath))
                    {
                        dataSet.ReadXml(mappingPath);
                        if (dataSet.Tables.Contains("TimeSheet"))
                        {
                            foreach (DataRow dr in dataSet.Tables["TimeSheet"].Rows)
                            {
                                foreach (var p in columnMappings)
                                {
                                    if (p.QuickbookFieldName == Convert.ToString(dr["QuickbookFieldName"]) && !string.IsNullOrEmpty(Convert.ToString(dr["UserFieldName"])))
                                    {
                                        p.UserFieldName = Convert.ToString(dr["UserFieldName"]);
                                    }
                                }
                            }
                        }
                    }
                    break;
                case TransactionType.Job:
                    if (File.Exists(mappingPath))
                    {
                        dataSet.ReadXml(mappingPath);
                        if (dataSet.Tables.Contains("CustomerJob"))
                        {
                            foreach (DataRow dr in dataSet.Tables["CustomerJob"].Rows)
                            {
                                foreach (var p in columnMappings)
                                {
                                    if (p.QuickbookFieldName == Convert.ToString(dr["QuickbookFieldName"]) && !string.IsNullOrEmpty(Convert.ToString(dr["UserFieldName"])))
                                    {
                                        p.UserFieldName = Convert.ToString(dr["UserFieldName"]);
                                    }
                                }
                            }
                        }
                    }
                    break;
                default:
                    break;
            }

        }


        public List<string> GetUserColumns(DataTable excelData)
        {
            var data = excelData.Columns.Cast<DataColumn>()
                                 .Select(x => x.ColumnName)
                                 .ToList();

            return data;
        }

        private static DataColumn getUserColumnName(string fieldName, DataColumnCollection dtColumns)
        {
            string fieldNameNormalized = NormalizeColumnName(fieldName);
            foreach (DataColumn dtCol in dtColumns)
            {
                string dtColNameNormalized = NormalizeColumnName(dtCol.ColumnName);
                if (fieldNameNormalized == dtColNameNormalized)
                {
                    return dtCol;
                }
            }

            return null;
        }

        private static string NormalizeColumnName(string columnName)
        {
            var normalized = columnName.ToLower().Replace(" ", "");
            return normalized;
        }

        public List<ImportFromExcelField> GetImportFromExcelFields(int transportType)
        {
            var fields = new List<ImportFromExcelField>();
            List<string> columns = new List<string>();
            switch (transportType)
            {
                case (int)TransactionType.TimeTracking:
                    columns = Common.PropertiesFromType(new TimeTracking());
                    break;
                case (int)TransactionType.Job:
                    columns = Common.PropertiesFromType(new CustomerJob());
                    break;
                default:
                    break;
            }

            for (int i = 0; i < columns.Count; i++)
            {
                fields.Add(new ImportFromExcelField()
                {
                    FieldName = columns[i],
                    Title = columns[i],
                    SpreadsheetFieldType = "string",
                    RealFieldType = 1,
                    Description = "Import Description",
                    IsRequired = true
                });
            }



            return fields;
        }
    }
}
