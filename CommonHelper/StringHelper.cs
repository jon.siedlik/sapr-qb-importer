﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonHelper
{
    public class StringHelper
    {
        public static string RemoveWhiteSpace(string str)
        {
            return string.Join("", str.Split(default(string[]), StringSplitOptions.RemoveEmptyEntries));
        }

        public static string ToString(object value)
        {
            if(value == null || value == DBNull.Value)
            {
                return string.Empty;
            }

            return Convert.ToString(value);
        }
    }
}
