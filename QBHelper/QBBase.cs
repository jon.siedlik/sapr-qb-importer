﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using CommonHelper;
using CommonHelper.Enums;
using Logs;
using QBXMLRP2ELib;

namespace QBHelper
{
    public class QBBase : IDisposable
    {
        protected QBConnector objCMIQBor;
        protected readonly RequestProcessor2 Rp2;
        protected string Response;
        protected readonly string Ticket;
        protected readonly XmlDocument Doc = new XmlDocument();
        private readonly List<SerializerHoler> _serializerHoler = new List<SerializerHoler>();

        protected QBBase(string connectionPath)
        {
            objCMIQBor = new QBConnector(connectionPath);
            Rp2 = objCMIQBor.RequestProcessor;
            Ticket = objCMIQBor.Ticket;
        }
        public void Dispose()
        {
            if (Rp2 != null)
            {
                Rp2.EndSession(Ticket);
                Rp2.CloseConnection();
                objCMIQBor.CloseConnection();
            }
        }
        protected List<GlobalValue.Reponse> Save<T>(QueryXML syncEntity, IList<T> syncObjectList, string[] includeRetElement = null)
        {
            Log.Add("Preparing " + syncEntity.EnumName() + " xml to save in quickBooks");

            StringBuilder sb = new StringBuilder();
            Type type = null;
            if (syncObjectList.Count > 0)
            {
                type = syncObjectList[0].GetType();
            }

            string includeRet = "";
            if (includeRetElement != null)
            {
                foreach (string s in includeRetElement)
                {
                    includeRet += "<IncludeRetElement>" + s + "</IncludeRetElement>\r\n";
                }
            }
            else
            {
                includeRet = "<IncludeRetElement>ListID</IncludeRetElement>\r\n";
                includeRet += "<IncludeRetElement>TxnID</IncludeRetElement>\r\n";
            }

            string objectToXml;
            string EntityName = syncEntity.EnumName();
            foreach (T syncObject in syncObjectList)
            {
                string xmlNodeName = GetEntityName(EntityName);
                XmlSerializerNamespaces emptyNamepsaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
                XmlRootAttribute xmlRootAttribute = new XmlRootAttribute
                {
                    ElementName = xmlNodeName + "Add"
                };
                XmlSerializer xs = GetSerializer(type, xmlRootAttribute);
                XmlWriterSettings settings = new XmlWriterSettings
                {
                    Indent = true,
                    OmitXmlDeclaration = true
                };
                using (var stream = new StringWriter())
                using (var writer = XmlWriter.Create(stream, settings))
                {
                    xs.Serialize(writer, syncObject, emptyNamepsaces);
                    objectToXml = stream.ToString();
                }

                objectToXml =
                    string.Format(
                        "<{0}{2}Rq>\n{1}\n{3}</{0}{2}Rq>",
                        xmlNodeName, objectToXml, "Add", includeRet);

                sb.AppendLine(objectToXml);
            }

            objectToXml = string.Format(
                "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<?qbxml version=\"13.0\"?>\n<QBXML>\n<QBXMLMsgsRq onError=\"continueOnError\">\n{0}\n</QBXMLMsgsRq>\n</QBXML>",
                sb);

            List<GlobalValue.Reponse> saveQBData = SaveQBData(objectToXml);
            Log.Add(saveQBData.Count(s => !s.IsError) + " " + syncEntity.EnumName() + " records are inserted in Quickbooks.");
            return saveQBData;
        }

        private string GetEntityName(string name)
        {
            switch (name)
            {
                case "CustomerAdd": return "Customer";
                case "ItemInventoryAdd": return "ItemInventory";
                case "ItemGroupAdd": return "ItemGroup";
                case "ItemServiceAdd": return "ItemService";
                case "PurchaseOrderAdd": return "PurchaseOrder";
                case "EstimateAdd": return "Estimate";
                default: return name;
            }
        }
        private List<GlobalValue.Reponse> SaveQBData(string query)
        {
            List<GlobalValue.Reponse> entityResponce = new List<GlobalValue.Reponse>();
            try
            {
                Log.Add("Sending  xml to save in quickBooks");
                Response = Rp2.ProcessRequest(Ticket, query);
                XmlDocument xDocument = new XmlDocument();
                xDocument.LoadXml(Response);
                XmlNode selectSingleNode = xDocument.SelectSingleNode("QBXML");
                if (selectSingleNode != null)
                {
                    XmlNode singleNode = selectSingleNode.SelectSingleNode("QBXMLMsgsRs");
                    if (singleNode != null)
                        foreach (XmlNode node in singleNode.ChildNodes)
                        {
                            if (node.Attributes != null)
                            {
                                GlobalValue.Reponse er = new GlobalValue.Reponse();
                                if (node.Attributes["statusSeverity"].Value == "Error")
                                {
                                    er.IsError = true;
                                    er.EntityObject = node.Name.Replace("AddRs", "");
                                    Log.Add("ERROR ON INSERT to QB: " + node.Attributes["statusMessage"].Value);
                                }
                                else
                                {
                                    er.EntityObject = node.InnerXml;
                                }
                                er.Message = node.Attributes["statusMessage"].Value;
                                entityResponce.Add(er);
                            }
                            else
                            {
                                entityResponce.Add(new GlobalValue.Reponse
                                {
                                    Message = "Invalid request.",
                                    IsError = true
                                });
                            }
                        }
                }
            }
            catch (COMException ex)
            {
                Log.Add(ex, "Error: " + ex.Message);
                entityResponce.Add(new GlobalValue.Reponse
                {
                    Message = ex.Message,
                    IsError = true,
                    EntityObject = query

                });
            }
            catch (Exception ex)
            {
                Log.Add(ex, "Error: " + ex.Message);
                entityResponce.Add(new GlobalValue.Reponse
                {
                    Message = ex.Message,
                    IsError = true
                });
            }

            return entityResponce;
        }
        protected List<GlobalValue.Reponse> Update<T>(QueryXML syncEntity, IList<T> syncObjectList, string[] includeRetElement = null)
        {

            Log.Add("Preparing " + syncEntity.EnumName() + " xml to update in quickBooks");
            StringBuilder sb = new StringBuilder();
            Type type = null;
            if (syncObjectList.Count > 0)
            {
                type = syncObjectList[0].GetType();
            }

            string includeRet = "";
            if (includeRetElement != null)
            {
                foreach (string s in includeRetElement)
                {
                    includeRet += "<IncludeRetElement>" + s + "</IncludeRetElement>\r\n";
                }
            }
            else
            {
                includeRet = "<IncludeRetElement>ListID</IncludeRetElement>\r\n";
                includeRet += "<IncludeRetElement>TxnID</IncludeRetElement>\r\n";
            }

            string objectToXml;
            foreach (T syncObject in syncObjectList)
            {
                string xmlNodeName = syncEntity.EnumName();
                XmlSerializerNamespaces emptyNamepsaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
                XmlRootAttribute xmlRootAttribute = new XmlRootAttribute
                {
                    ElementName = xmlNodeName + "Mod"
                };
                XmlSerializer xs = GetSerializer(type, xmlRootAttribute);
                XmlWriterSettings settings = new XmlWriterSettings
                {
                    Indent = true,
                    OmitXmlDeclaration = true
                };
                using (var stream = new StringWriter())
                using (var writer = XmlWriter.Create(stream, settings))
                {
                    xs.Serialize(writer, syncObject, emptyNamepsaces);
                    objectToXml = stream.ToString();
                }

                objectToXml =
                    string.Format(
                        "<{0}{2}Rq>\n{1}\n{3}</{0}{2}Rq>",
                        xmlNodeName, objectToXml, "Mod", includeRet);

                sb.AppendLine(objectToXml);
            }

            objectToXml = string.Format(
                "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<?qbxml version=\"9.0\"?>\n<QBXML>\n<QBXMLMsgsRq onError=\"continueOnError\">\n{0}\n</QBXMLMsgsRq>\n</QBXML>",
                sb);

            List<GlobalValue.Reponse> saveQBData = SaveQBData(objectToXml);
            Log.Add(saveQBData.Count(s => !s.IsError) + " " + syncEntity.EnumName() + " records are inserted in Quickbooks.");
            return saveQBData;
        }
        protected List<T> GetQBData<T>(QueryXML query, string[] includeRetElements = null, DateTime? fromDate = null, bool isTxnDate = false, DateTime? toDate = null)
        {
            if (objCMIQBor.SessionBegun)
            {
                string queryXml = query.EnumDescription();

                //Retrive records accroding to dateModified------------------
                string fromDateParameter = "";
                if (fromDate.HasValue)
                {
                    if (isTxnDate)
                    {
                        fromDateParameter = "<TxnDateRangeFilter>\r\n";
                        if (DateTime.Now.IsDaylightSavingTime()) fromDate = fromDate.Value.AddHours(-1);
                        fromDateParameter += "<FromTxnDate>" + fromDate.Value.AddSeconds(1).ToString("yyyy-MM-dd") + "</FromTxnDate>\r\n";
                        fromDateParameter += "<ToTxnDate>";
                        if (toDate.HasValue)
                        {
                            fromDateParameter += toDate.Value.ToString("yyyy-MM-dd");
                        }
                        else
                        {
                            fromDateParameter += DateTime.Now.ToString("yyyy-MM-dd");
                        }
                        fromDateParameter += "</ToTxnDate>\r\n";
                        fromDateParameter += "</TxnDateRangeFilter>\r\n";
                    }
                    else
                    {
                        if (DateTime.Now.IsDaylightSavingTime()) fromDate = fromDate.Value.AddHours(-1);
                        fromDateParameter = "<FromModifiedDate>" + fromDate.Value.AddSeconds(1).ToString("yyyy-MM-ddTHH:mm:ss") + "</FromModifiedDate>\r\n";
                        if (toDate.HasValue)
                        {
                            fromDateParameter += "<ToModifiedDate>" + toDate.Value.ToString("yyyy-MM-ddTHH:mm:ss") + "</ToModifiedDate>\r\n";
                        }
                        else
                        {
                            fromDateParameter += "<ToModifiedDate>" + DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss") + "</ToModifiedDate>\r\n";
                        }
                    }
                }

                ////-----------------------------------------------------------
                queryXml = queryXml.Replace(SyncConstant.PARAMETER_CAPTION, fromDateParameter);

                Log.Add("Getting " + query.EnumName() + " from QuickBooks");
                Response = Rp2.ProcessRequest(Ticket, queryXml);

                string xml = SyncConstant.GenerateXML(Response);
                List<T> lstcust = SyncConstant.SerializeXml<T>(xml, query);

                Log.Add(lstcust.Count + " " + query.EnumName() + " found in quickBooks" + (fromDate.HasValue ? (" having modified date greater then " + fromDate.Value) : "."));

                return lstcust;
            }
            else
            {
                throw new Exception("Could not start QuickBooks");
            }
        }
        protected List<T> GetQBDataQuery<T>(QueryXML query, string listID)
        {
            if (objCMIQBor.SessionBegun)
            {
                string queryXml = query.EnumDescription();

                //Retrive records accroding to dateModified------------------
                string _listID = "";
                _listID = "<ListID>" + listID + "</ListID>\r\n";

                ////-----------------------------------------------------------

                queryXml = queryXml.Replace(SyncConstant.PARAMETER_CAPTION, _listID);

                Log.Add("Getting " + query.EnumName() + " from QuickBooks");
                Response = Rp2.ProcessRequest(Ticket, queryXml);

                string xml = SyncConstant.GenerateXML(Response);
                List<T> lstcust = SyncConstant.SerializeXml<T>(xml, query);
                if (lstcust.Count == 0)
                {
                    var serializer = new XmlSerializer(typeof(ErrorResponse));
                }
                return lstcust;
            }
            else
            {
                throw new Exception("Could not start QuickBooks");
            }
        }
        protected List<T> GetQBData<T>(QueryXML query)
        {
            if (objCMIQBor.SessionBegun)
            {
                string queryXml = query.EnumDescription();

                queryXml = queryXml.Replace(SyncConstant.PARAMETER_CAPTION, "");

                Log.Add("Getting " + query.EnumName() + " from QuickBooks");
                Response = Rp2.ProcessRequest(Ticket, queryXml);

                string xml = SyncConstant.GenerateXML(Response);
                List<T> list = SyncConstant.SerializeXml<T>(xml, query);

                Log.Add(list.Count + " " + query.EnumName() + " found in quickBooks.");

                return list;
            }
            else
            {
                throw new Exception("Could not start QuickBooks");
            }
        }
        protected List<T> GetQBData<T>(QueryXML query, DateTime? fromDate = null, string searchName = null)
        {
            string queryXml = query.EnumDescription();

            //Retrive records accroding to dateModified------------------
            string fromDateParameter = "";
            if (fromDate.HasValue)
            {
                if (DateTime.Now.IsDaylightSavingTime()) fromDate = fromDate.Value.AddHours(-1);
                fromDateParameter = "<FromModifiedDate>" + fromDate.Value.AddSeconds(1).ToString("yyyy-MM-ddTHH:mm:ss") + "</FromModifiedDate>\r\n" +
                               "<ToModifiedDate>" + DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss") + "</ToModifiedDate>\r\n";
                //if (query == QueryXML.Invoice || query == QueryXML.Bill || query == QueryXML.ReceivePayment)
                //    fromDateParameter = "<ModifiedDateRangeFilter>" + fromDateParameter + "</ModifiedDateRangeFilter>";
            }
            //if (query == QueryXML.CustomerSearch)
            //    fromDateParameter = "<FullName>" + searchName + "</FullName>\r\n";
            ////-----------------------------------------------------------

            queryXml = queryXml.Replace(SyncConstant.PARAMETER_CAPTION, fromDateParameter);

            //GlobalValue.Eventlogupdate("Getting " + query.EnumName() + " from QuickBooks", null);
            Response = Rp2.ProcessRequest(Ticket, queryXml);

            string xml = SyncConstant.GenerateXML(Response);
            List<T> lstcust = SyncConstant.SerializeXml<T>(xml, query);

            //GlobalValue.Eventlogupdate(lstcust.Count + " " + query.EnumName() + " found in quickBooks" + (fromDate.HasValue ? (" having modified date greater then " + fromDate.Value) : "."), null);

            return lstcust;
        }
        private XmlSerializer GetSerializer(Type type, XmlRootAttribute root)
        {
            foreach (SerializerHoler sh in _serializerHoler)
            {
                if (sh.Type == type && sh.XmlRootAttribute.ElementName == root.ElementName)
                {
                    return sh.XmlSerializer;
                }
            }
            XmlSerializer xs = new XmlSerializer(type, root);
            _serializerHoler.Add(new SerializerHoler
            {
                XmlSerializer = xs,
                XmlRootAttribute = root,
                Type = type
            });
            return xs;
        }
        private class SerializerHoler
        {
            public XmlSerializer XmlSerializer { get; set; }
            public Type Type { get; set; }
            public XmlRootAttribute XmlRootAttribute { get; set; }
        }
        public class ErrorResponse
        {
            public string StatusMessage { get; set; }
        }

    }
}
