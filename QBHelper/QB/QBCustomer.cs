﻿using CommonHelper;
using CommonHelper.Enums;
using QBHelper.ModelAdd;
using QBHelper.ModelRet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QBHelper.QB
{
    public class QBCustomer : QBBase
    {
        public QBCustomer(string connectionPath) : base(connectionPath)
        {

        }
        private static List<CustomerRet> _fg;
        public IList<CustomerRet> GetAllSyncCustomers(DateTime? qbAccountLastMod = null)
        {
            _fg = GetQBData<CustomerRet>(QueryXML.Customer, qbAccountLastMod);
            return _fg;
        }
        public IList<CustomerRet> SearchCustomer(String name)
        {
            _fg = GetQBData<CustomerRet>(QueryXML.Customer, null, name);
            return _fg;
        }

        public List<GlobalValue.Reponse> SaveCustomerJobs(List<CustomerAdd> customerRets)
        {
            string[] includeRetElement = Common.GetPropertyValues<CustomerRet>(new CustomerRet()).ToArray();
            List<GlobalValue.Reponse> reponses = Save(QueryXML.CustomerAdd, customerRets, includeRetElement);
            return reponses;
        }
    }
}
