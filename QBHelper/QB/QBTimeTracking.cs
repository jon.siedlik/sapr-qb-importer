﻿using CommonHelper;
using CommonHelper.Enums;
using QBHelper.ModelAdd;
using QBHelper.ModelMod;
using QBHelper.ModelRet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QBHelper.QB
{
    public class QBTimeTracking : QBBase
    {
        public QBTimeTracking(string connectionPath) : base(connectionPath)
        {

        }
        public List<GlobalValue.Reponse> SaveTimeTracking(List<TimeTrackingAdd> timetracks)
        {
            List<GlobalValue.Reponse> reponses = Save(QueryXML.TimeTracking, timetracks);
            return reponses;
        }

        public List<GlobalValue.Reponse> UpdateTimeTracking(List<TimeTrackingMod> timetracks)
        {
            List<GlobalValue.Reponse> reponses = Update(QueryXML.TimeTracking, timetracks);
            return reponses;
        }

        public List<TimeTrackingRet> getAllTimeTracks()
        {
            List<TimeTrackingRet> reponses = GetQBData<TimeTrackingRet>(QueryXML.TimeTrackingQuery);
            return reponses;
        }
        public List<TimeTrackingRet> getAllTimeTracks(DateTime from, DateTime to)
        {
            List<TimeTrackingRet> reponses = GetQBData<TimeTrackingRet>(QueryXML.TimeTrackingQuery, null, from, true, to);
            return reponses;
        }
    }
}
