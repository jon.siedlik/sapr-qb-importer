﻿using CommonHelper;
using CommonHelper.Enums;
using QBHelper.ModelAdd;
using QBHelper.ModelRet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QBHelper.QB
{
    public class QBEmployee : QBBase
    {
        public QBEmployee(string connectionPath) : base(connectionPath)
        {

        }
        public List<EmployeeRet> GetAllEmployee(DateTime? fromDate = null)
        {
            List<EmployeeRet> employeeAdds = GetQBData<EmployeeRet>(QueryXML.EmployeeQuery, null, fromDate);
            return employeeAdds;
        }

        public List<EmployeeRet> GetEmployeeQuery(string listID)
        {
            List<EmployeeRet> employeeAdds = GetQBDataQuery<EmployeeRet>(QueryXML.EmployeeQuery, listID);
            return employeeAdds;
        }

        public List<GlobalValue.Reponse> SaveEmployees(List<EmployeeAdd> employees)
        {
            List<GlobalValue.Reponse> reponses = Save(QueryXML.Employee, employees);
            return reponses;
        }
    }
}
