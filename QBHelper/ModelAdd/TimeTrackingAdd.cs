﻿using QBHelper.Ref;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace QBHelper.ModelAdd
{
    [XmlTypeAttribute(AnonymousType = true)]
    [XmlRootAttribute(Namespace = "", IsNullable = false)]
    public class TimeTrackingAdd
    {
        public string TxnDate { get; set; }

        public EntityRef EntityRef { get; set; }

        public CustomerRef CustomerRef { get; set; }

        public ItemServiceRef ItemServiceRef { get; set; }

        public string Duration { get; set; }

        public ClassRef ClassRef { get; set; }

        public PayrollItemWageRef PayrollItemWageRef { get; set; }

        public string Notes { get; set; }

        public string BillableStatus { get; set; }

        public string IsBillable { get; set; }

        public string ExternalGUID { get; set; }

    }
}
