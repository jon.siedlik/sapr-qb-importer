﻿using QBHelper.Ref;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace QBHelper.ModelAdd
{
    [XmlTypeAttribute(AnonymousType = true)]
    [XmlRootAttribute(Namespace = "", IsNullable = false)]
    public class CustomerAdd
    {
        public string Name { get; set; }
        public string IsActive { get; set; }
        public ClassRef ClassRef { get; set; }
        public ParentRef ParentRef { get; set; }
        public string CompanyName { get; set; }
        public string Salutation { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string JobTitle { get; set; }
        [XmlElement("BillAddress")]
        public BillAddress BillAddress { get; set; }
        [XmlElement("ShipAddress")]
        public ShipAddress ShipAddress { get; set; }
        public ShipToAddress ShipToAddress { get; set; }
        public string Phone { get; set; }
        public string AltPhone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Cc { get; set; }
        public string Contact { get; set; }
        public string AltContact { get; set; }
        [XmlElement("AdditionalContactRef")]
        public List<AdditionalContactRef> AdditionalContactRef { get; set; }
        [XmlElement("Contacts")]
        public List<Contacts> Contacts { get; set; }
        public CustomerTypeRef CustomerTypeRef { get; set; }
        public TermsRef TermsRef { get; set; }
        public SalesRepRef SalesRepRef { get; set; }
        public string OpenBalance { get; set; }
        public string OpenBalanceDate { get; set; }
        public SalesTaxCodeRef SalesTaxCodeRef { get; set; }
        public ItemSalesTaxRef ItemSalesTaxRef { get; set; }
        public string SalesTaxCountry { get; set; }
        public string ResaleNumber { get; set; }
        public string AccountNumber { get; set; }
        public string CreditLimit { get; set; }
        public CreditCardInfo CreditCardInfo { get; set; }
        public string JobStatus { get; set; }
        public string JobStartDate { get; set; }
        public string JobProjectedEndDate { get; set; }
        public string JobEndDate { get; set; }
        public string JobDesc { get; set; }
        public JobTypeRef JobTypeRef { get; set; }
        public string Notes { get; set; }
        [XmlElement("AdditionalNotes")]
        public List<AdditionalNotes> AdditionalNotes { get; set; }
        public string PreferredDeliveryMethod { get; set; }
        public PriceLevelRef PriceLevelRef { get; set; }
        public string ExternalGUID { get; set; }
        public string TaxRegistrationNumber { get; set; }
        public string CurrencyRef { get; set; }
    }
}
