﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QBHelper.ModelAdd
{
    public class EmployeeAdd
    {
        public int IsActive { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
    }

    public class EmployeePayrollInfo
    {
        public int IsUsingTimeDataToCreatePaychecks { get; set; }
        public string UseTimeDataToCreatePaychecks { get; set; }
    }
}
