﻿using Logs;
using QBFC13Lib;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QBHelper
{
	public class QBCompanySettings
	{
		private Hashtable myDataExtHT;

		private static string strOwnerID = null;

		public string this[string keyName]
		{
			get
			{
				return this.GetKey(keyName);
			}
			set
			{
				this.PutKey(keyName, value);
			}
		}

		public int Count
		{
			get
			{
				return (this.myDataExtHT == null) ? 0 : this.myDataExtHT.Count;
			}
		}

		public static string StrOwnerID
		{
			get
			{
				return QBCompanySettings.strOwnerID;
			}
			set
			{
				QBCompanySettings.strOwnerID = ((value == null) ? "" : value);
			}
		}

		public QBCompanySettings(string ownerID)
		{
			QBCompanySettings.strOwnerID = ownerID;
			this.Open();
		}

		private void Open()
		{
			this.myDataExtHT = new Hashtable();
			try
			{
				IMsgSetRequest msgSetRequest = QBCommunication.QBSessionManager.CreateMsgSetRequest("US", QBCommunication.QBXmlMajorVersion, QBCommunication.QBXmlMinorVersion);
				ICompanyQuery companyQuery = msgSetRequest.AppendCompanyQueryRq();
				companyQuery.OwnerIDList.Add(QBCompanySettings.strOwnerID);
				IMsgSetResponse msgSetResponse = QBCommunication.QBSessionManager.DoRequests(msgSetRequest);
				LogQB.Add("", msgSetRequest, msgSetResponse);
				ICompanyRet companyRet = (ICompanyRet)msgSetResponse.ResponseList.GetAt(0).Detail;
				bool flag = companyRet.DataExtRetList != null;
				if (flag)
				{
					for (int i = 0; i < companyRet.DataExtRetList.Count; i++)
					{
						IDataExtRet at = companyRet.DataExtRetList.GetAt(i);
						string text = at.DataExtValue.GetValue();
						bool flag2 = text.Length > 0;
						if (flag2)
						{
							text = text.Remove(0, 1);
						}
						bool flag3 = text.Length > 0;
						if (flag3)
						{
							text = text.Remove(text.Length - 1, 1);
						}
						this.myDataExtHT.Add(at.DataExtName.GetValue(), text);
					}
				}
			}
			catch (Exception ex)
			{
				Log.Add(ex);
			}
		}

		private string GetKey(string keyName)
		{
			return (this.myDataExtHT[keyName] == null) ? null : this.myDataExtHT[keyName].ToString();
		}

		private void PutKey(string keyName, string keyValue)
		{
			bool flag = this.myDataExtHT[keyName] == null;
			if (flag)
			{
				this.QBInsertKey(keyName, keyValue);
			}
			else
			{
				this.QBModifyKey(keyName, keyValue);
			}
		}

		private void QBInsertKey(string keyName, string keyValue)
		{
			try
			{
				IMsgSetRequest msgSetRequest = QBCommunication.QBSessionManager.CreateMsgSetRequest("US", QBCommunication.QBXmlMajorVersion, QBCommunication.QBXmlMinorVersion);
				msgSetRequest.Attributes.OnError = ENRqOnError.roeContinue;
				IDataExtDefAdd dataExtDefAdd = msgSetRequest.AppendDataExtDefAddRq();
				dataExtDefAdd.AssignToObjectList.Add(ENAssignToObject.atoCompany);
				dataExtDefAdd.DataExtType.SetValue(ENDataExtType.detSTR1024TYPE);
				dataExtDefAdd.DataExtName.SetValue(keyName);
				dataExtDefAdd.OwnerID.SetValue(QBCompanySettings.strOwnerID);
				IDataExtAdd dataExtAdd = msgSetRequest.AppendDataExtAddRq();
				dataExtAdd.DataExtName.SetValue(keyName);
				string value = string.Format("'{0}'", keyValue);
				dataExtAdd.DataExtValue.SetValue(value);
				dataExtAdd.OwnerID.SetValue(QBCompanySettings.strOwnerID);
				dataExtAdd.ORListTxnWithMacro.OtherDataExtType.SetValue(ENOtherDataExtType.odetCompany);
				IMsgSetResponse msgSetResponse = QBCommunication.QBSessionManager.DoRequests(msgSetRequest);
				LogQB.Add("", msgSetRequest, msgSetResponse);
				bool flag = msgSetResponse.ResponseList.GetAt(1).StatusCode == 0;
				if (flag)
				{
					this.myDataExtHT.Add(keyName, keyValue);
				}
			}
			catch (Exception ex)
			{
				Log.Add(ex);
			}
		}

		private void QBModifyKey(string keyName, string keyValue)
		{
			try
			{
				IMsgSetRequest msgSetRequest = QBCommunication.QBSessionManager.CreateMsgSetRequest("US", QBCommunication.QBXmlMajorVersion, QBCommunication.QBXmlMinorVersion);
				msgSetRequest.Attributes.OnError = ENRqOnError.roeStop;
				IDataExtMod dataExtMod = msgSetRequest.AppendDataExtModRq();
				dataExtMod.DataExtName.SetValue(keyName);
				string value = string.Format("'{0}'", keyValue);
				dataExtMod.DataExtValue.SetValue(value);
				dataExtMod.OwnerID.SetValue(QBCompanySettings.strOwnerID);
				dataExtMod.ORListTxn.OtherDataExtType.SetValue(ENOtherDataExtType.odetCompany);
				IMsgSetResponse msgSetResponse = QBCommunication.QBSessionManager.DoRequests(msgSetRequest);
				LogQB.Add("", msgSetRequest, msgSetResponse);
				bool flag = msgSetResponse.ResponseList.GetAt(0).StatusCode == 0;
				if (flag)
				{
					this.myDataExtHT[keyName] = keyValue;
				}
			}
			catch (Exception ex)
			{
				Log.Add(ex);
			}
		}
	}
}
