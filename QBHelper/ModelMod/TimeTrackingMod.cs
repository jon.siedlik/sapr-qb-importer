﻿using QBHelper.Ref;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace QBHelper.ModelMod
{
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class TimeTrackingMod
    {
        public string TxnID { get; set; }
        public string EditSequence { get; set; }
        public string TxnDate { get; set; }
        public EntityRef EntityRef { get; set; }
        public CustomerRef CustomerRef { get; set; }
        public ItemServiceRef ItemServiceRef { get; set; }
        public string Duration { get; set; }
        public ClassRef ClassRef { get; set; }
        public PayrollItemWageRef PayrollItemWageRef { get; set; }
        public string Notes { get; set; }
        public string BillableStatus { get; set; }
    }
}
