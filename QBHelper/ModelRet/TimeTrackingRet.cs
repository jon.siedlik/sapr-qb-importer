﻿using QBHelper.Ref;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace QBHelper.ModelRet
{
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public partial class TimeTrackingRet
    {
        public string TxnID { get; set; }
        public DateTime TimeCreated { get; set; }
        public DateTime TimeModified { get; set; }
        public string EditSequence { get; set; }
        public int TxnNumber { get; set; }
        public DateTime TxnDate { get; set; }
        public EntityRef EntityRef { get; set; }
        public CustomerRef CustomerRef { get; set; }
        public ItemServiceRef ItemServiceRef { get; set; }
        public string Duration { get; set; }
        public ClassRef ClassRef { get; set; }
        public PayrollItemWageRef PayrollItemWageRef { get; set; }
        public string Notes { get; set; }
        public string BillableStatus { get; set; }
        public bool IsBillable { get; set; }
        public bool isBilled { get; set; }
    }
}
