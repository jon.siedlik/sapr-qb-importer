﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using QBHelper.Ref;

namespace QBHelper.ModelRet
{
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class EmployeeRet
    {
        public string ListID { get; set; }
        public DateTime TimeCreated { get; set; }
        public DateTime TimeModified { get; set; }
        public string EditSequence { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public string Salutation { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string JobTitle { get; set; }
        public SupervisorRef SupervisorRef { get; set; }
        public string Department { get; set; }
        public string Description { get; set; }
        public string EmployeeType { get; set; }
        public string AccountNumber { get; set; }
        [XmlElement("EmployeePayrollInfo")]
        public EmployeePayrollInfo EmployeePayrollInfo { get; set; }

        [XmlElement("DataExtRet")]
        public List<DataExtRet> DataExtRet { get; set; }


    }

    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class EmployeePayrollInfo
    {
        public bool IsUsingTimeDataToCreatePaychecks { get; set; }
        public string UseTimeDataToCreatePaychecks { get; set; }
    }
}
