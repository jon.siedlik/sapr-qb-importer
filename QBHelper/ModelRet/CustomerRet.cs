﻿using QBHelper.Ref;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace QBHelper.ModelRet
{
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public partial class CustomerRet
    {
        public string ListID { get; set; }
        public DateTime? TimeCreated { get; set; }
        public DateTime? TimeModified { get; set; }
        public string EditSequence { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        public bool? IsActive { get; set; }
        public ClassRef ClassRef { get; set; }
        public ParentRef ParentRef { get; set; }
        public int Sublevel { get; set; }
        public string CompanyName { get; set; }
        public string Salutation { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string JobTitle { get; set; }
        public BillAddress BillAddress { get; set; }
        public BillAddressBlock BillAddressBlock { get; set; }
        public ShipAddress ShipAddress { get; set; }
        public ShipAddressBlock ShipAddressBlock { get; set; }
        public ShipToAddress ShipToAddress { get; set; }
        public string Phone { get; set; }
        public string AltPhone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Cc { get; set; }
        public string Contact { get; set; }
        public string AltContact { get; set; }

        [XmlElement("AdditionalContactRef")]
        public List<AdditionalContactRef> AdditionalContactRef { get; set; }


    }
}
