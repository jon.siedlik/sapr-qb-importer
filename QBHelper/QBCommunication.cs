﻿using Logs;
using QBFC13Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace QBHelper
{
	public class QBCommunication
	{
		protected static short qbXmlMajorVersion;

		protected static short qbXmlMinorVersion;

		protected static QBCommunication instance;

		protected QBSessionManagerClass qbSessionManager;

		public static short QBXmlMajorVersion
		{
			get
			{
				return QBCommunication.qbXmlMajorVersion;
			}
			set
			{
				QBCommunication.qbXmlMajorVersion = value;
			}
		}

		public static short QBXmlMinorVersion
		{
			get
			{
				return QBCommunication.qbXmlMinorVersion;
			}
			set
			{
				QBCommunication.qbXmlMinorVersion = value;
			}
		}

		public static QBCommunication Instance
		{
			get
			{
				return QBCommunication.instance;
			}
		}

		public static QBSessionManagerClass QBSessionManager
		{
			get
			{
				return QBCommunication.Instance.qbSessionManager;
			}
			set
			{
				QBCommunication.Instance.qbSessionManager = value;
			}
		}

		public QBCommunication()
		{
			this.qbSessionManager = null;
			QBCommunication.QBXmlMajorVersion = 0;
			QBCommunication.QBXmlMinorVersion = 0;
		}

		public QBCommunication(short qbXmlMajorVersion)
		{
			this.qbSessionManager = null;
			QBCommunication.QBXmlMajorVersion = qbXmlMajorVersion;
			QBCommunication.QBXmlMinorVersion = 0;
		}

		public QBCommunication(short qbXmlMajorVersion, short qbXmlMinorVersion)
		{
			this.qbSessionManager = null;
			QBCommunication.QBXmlMajorVersion = qbXmlMajorVersion;
			QBCommunication.QBXmlMinorVersion = qbXmlMinorVersion;
		}

		public static void CreateInstance()
		{
			QBCommunication.instance = new QBCommunication();
		}

		public static void CreateInstance(short qbXmlMajorVersion)
		{
			QBCommunication.instance = new QBCommunication(qbXmlMajorVersion);
		}

		public static void CreateInstance(short qbXmlMajorVersion, short qbXmlMinorVersion)
		{
			QBCommunication.instance = new QBCommunication(qbXmlMajorVersion, qbXmlMinorVersion);
		}

		public static bool IsCreated()
		{
			return QBCommunication.instance != null;
		}

		public static bool InitManager(string appName)
		{
			bool flag = !QBCommunication.IsCreated();
			bool result;
			if (flag)
			{
				result = false;
			}
			else
			{
				try
				{
					QBCommunication.Instance.qbSessionManager = new QBSessionManagerClass();
					QBCommunication.Instance.qbSessionManager.OpenConnection("", appName);
					QBCommunication.Instance.qbSessionManager.BeginSession("", ENOpenMode.omDontCare);
				}
				catch
				{
					Thread.Sleep(500);
					try
					{
						QBCommunication.Instance.qbSessionManager = new QBSessionManagerClass();
						QBCommunication.Instance.qbSessionManager.OpenConnection("", appName);
						QBCommunication.Instance.qbSessionManager.BeginSession("", ENOpenMode.omDontCare);
					}
					catch (Exception ex)
					{
						Log.Add(ex);
						QBCommunication.Instance.qbSessionManager = null;
						result = false;
						return result;
					}
					result = true;
					return result;
				}
				result = true;
			}
			return result;
		}

		public static bool InitManager(string appName, string qbFile)
		{
			bool flag = !QBCommunication.IsCreated();
			bool result;
			if (flag)
			{
				result = false;
			}
			else
			{
				try
				{
					QBCommunication.Instance.qbSessionManager = new QBSessionManagerClass();
					QBCommunication.Instance.qbSessionManager.OpenConnection("", appName);
					QBCommunication.Instance.qbSessionManager.BeginSession(qbFile, ENOpenMode.omDontCare);
				}
				catch (Exception ex)
				{
					Log.Add(ex);
					QBCommunication.Instance.qbSessionManager = null;
					result = false;
					return result;
				}
				result = true;
			}
			return result;
		}

		public static bool InitManager(string appName, ENOpenMode openMode)
		{
			bool flag = !QBCommunication.IsCreated();
			bool result;
			if (flag)
			{
				result = false;
			}
			else
			{
				try
				{
					QBCommunication.Instance.qbSessionManager = new QBSessionManagerClass();
					QBCommunication.Instance.qbSessionManager.OpenConnection("", appName);
					QBCommunication.Instance.qbSessionManager.BeginSession("", openMode);
				}
				catch (Exception ex)
				{
					Log.Add(ex);
					QBCommunication.Instance.qbSessionManager = null;
					result = false;
					return result;
				}
				result = true;
			}
			return result;
		}

		public static bool InitManager(string appName, string qbFile, ENOpenMode openMode)
		{
			bool flag = !QBCommunication.IsCreated();
			bool result;
			if (flag)
			{
				result = false;
			}
			else
			{
				try
				{
					QBCommunication.Instance.qbSessionManager = new QBSessionManagerClass();
					QBCommunication.Instance.qbSessionManager.OpenConnection("", appName);
					QBCommunication.Instance.qbSessionManager.BeginSession(qbFile, openMode);
				}
				catch (Exception ex)
				{
					Log.Add(ex);
					QBCommunication.Instance.qbSessionManager = null;
					result = false;
					return result;
				}
				result = true;
			}
			return result;
		}

		public static bool InitManager(string appID, string appName, string qbFile, ENOpenMode openMode, ENConnectionType connectionType)
		{
			bool flag = !QBCommunication.IsCreated();
			bool result;
			if (flag)
			{
				result = false;
			}
			else
			{
				try
				{
					QBCommunication.Instance.qbSessionManager = new QBSessionManagerClass();
					QBCommunication.Instance.qbSessionManager.OpenConnection2(appID, appName, connectionType);
					QBCommunication.Instance.qbSessionManager.BeginSession(qbFile, openMode);
				}
				catch (Exception ex)
				{
					Log.Add(ex);
					QBCommunication.Instance.qbSessionManager = null;
					result = false;
					return result;
				}
				result = true;
			}
			return result;
		}

		public static void InitRegistrationManager(string appName)
		{
			bool flag = false;
			int num = 0;
			while (!flag && num < 5)
			{
				try
				{
					QBCommunication.Instance.qbSessionManager = new QBSessionManagerClass();
					QBCommunication.Instance.qbSessionManager.OpenConnection("", appName);
					flag = true;
				}
				catch (Exception ex)
				{
					Log.Add(ex);
					QBCommunication.Instance.qbSessionManager = null;
					num++;
				}
			}
		}

		public static void ReleaseManager()
		{
			bool flag = !QBCommunication.IsCreated();
			if (!flag)
			{
				try
				{
					bool flag2 = QBCommunication.Instance.qbSessionManager != null;
					if (flag2)
					{
						try
						{
							QBCommunication.Instance.qbSessionManager.EndSession();
							QBCommunication.Instance.qbSessionManager.CloseConnection();
						}
						catch (Exception ex)
						{
							Log.Add(ex);
						}
						finally
						{
							QBCommunication.Instance.qbSessionManager.CloseConnection();
						}
					}
				}
				catch
				{
				}
			}
		}

		public static void ReleaseRegistrationManager()
		{
			try
			{
				bool flag = QBCommunication.Instance.qbSessionManager != null;
				if (flag)
				{
					try
					{
						QBCommunication.Instance.qbSessionManager.CloseConnection();
					}
					catch (Exception ex)
					{
						Log.Add(ex);
					}
					finally
					{
						QBCommunication.Instance.qbSessionManager = null;
					}
				}
			}
			catch
			{
			}
		}
	}
}
