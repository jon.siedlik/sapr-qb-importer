﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace QBHelper.Ref
{
    [XmlRoot(ElementName = "ClassRef")]
    public class ClassRef : BaseRef
    { }

    [XmlRoot(ElementName = "SalesRepRef")]
    public class ItemServiceRef : BaseRef
    { }

    [XmlRoot(ElementName = "CustomerRef")]
    public class CustomerRef : BaseRef
    { }

    [XmlRoot(ElementName = "EntityRef")]
    public class EntityRef : BaseRef
    { }

    [XmlRoot(ElementName = "PayrollItemWageRef")]
    public class PayrollItemWageRef : BaseRef
    { }

    [XmlRoot(ElementName = "ParentRef")]
    public class ParentRef : BaseRef
    { }

    [XmlRoot(ElementName = "BillAddress")]
    public partial class BillAddress
    {
        public string Addr1
        {
            get; set;
        }

        /// <remarks/>
        public string Addr2
        {
            get; set;
        }

        /// <remarks/>
        public string Addr3
        {
            get; set;
        }

        /// <remarks/>
        public string Addr4
        {
            get; set;
        }

        /// <remarks/>
        public string Addr5
        {
            get; set;
        }

        /// <remarks/>
        public string City
        {
            get; set;
        }

        /// <remarks/>
        public string State
        {
            get; set;
        }

        /// <remarks/>
        public string PostalCode
        {
            get; set;
        }

        /// <remarks/>
        public string Country
        {
            get; set;
        }

        public string Note
        {
            get; set;
        }
    }

    [XmlRoot(ElementName = "BillAddressBlock")]
    public partial class BillAddressBlock
    {
        public string Addr1 { get; set; }
        public string Addr2 { get; set; }
        public string Addr3 { get; set; }
        public string Addr4 { get; set; }
        public string Addr5 { get; set; }
    }

    /// <remarks/>
    [XmlRoot(ElementName = "ShipAddress")]
    public partial class ShipAddress
    {
        public string Addr1 { get; set; }
        public string Addr2 { get; set; }
        public string Addr3 { get; set; }
        public string Addr4 { get; set; }
        public string Addr5 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string Note { get; set; }
    }

    /// <remarks/>
    [XmlRoot(ElementName = "ShipAddressBlock")]
    public partial class ShipAddressBlock
    {
        public string Addr1 { get; set; }
        public string Addr2 { get; set; }
        public string Addr3 { get; set; }
        public string Addr4 { get; set; }
        public string Addr5 { get; set; }
    }

    [XmlRoot(ElementName = "ShipToAddress")]
    public partial class ShipToAddress
    {
        public string Name { get; set; }
        public string Addr1 { get; set; }
        public string Addr2 { get; set; }
        public string Addr3 { get; set; }
        public string Addr4 { get; set; }
        public string Addr5 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string Note { get; set; }

        public string DefaultShipTo { get; set; }

    }

    [XmlRoot(ElementName = "SupervisorRef")]
    public partial class SupervisorRef : BaseRef
    {

    }

    [XmlType(AnonymousType = true)]
    public partial class AdditionalContactRef
    {
        public string ContactName { get; set; }
        public string ContactValue { get; set; }
    }

    [XmlType(AnonymousType = true)]
    public partial class Contacts
    {
        public string Salutation { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string JobTitle { get; set; }
        [XmlElement("AdditionalContactRef")]
        public List<AdditionalContactRef> AdditionalContactRef { get; set; }
    }

    [XmlType(AnonymousType = true)]
    public partial class CustomerTypeRef : BaseRef
    { }

    [XmlType(AnonymousType = true)]
    public partial class TermsRef : BaseRef
    { }

    [XmlType(AnonymousType = true)]
    public partial class SalesRepRef : BaseRef
    { }

    [XmlType(AnonymousType = true)]
    public partial class SalesTaxCodeRef : BaseRef
    { }

    [XmlType(AnonymousType = true)]
    public partial class ItemSalesTaxRef : BaseRef
    { }

    [XmlType(AnonymousType = true)]
    public partial class PreferredPaymentMethodRef : BaseRef
    { }
    [XmlType(AnonymousType = true)]
    public partial class CreditCardInfo
    {
        public string CreditCardNumber { get; set; }
        public string ExpirationMonth { get; set; }
        public string ExpirationYear { get; set; }
        public string NameOnCard { get; set; }
        public string CreditCardAddress { get; set; }
        public string CreditCardPostalCode { get; set; }
    }

    [XmlType(AnonymousType = true)]
    public partial class JobTypeRef : BaseRef
    {

    }

    [XmlType(AnonymousType = true)]
    public partial class AdditionalNotes
    {
        public string Note { get; set; }
    }

    [XmlType(AnonymousType = true)]
    public partial class PriceLevelRef : BaseRef
    { }

    [XmlType(AnonymousType = true)]
    public partial class DataExtRet
    {
        public string OwnerID { get; set; }
        public string DataExtName { get; set; }
        public string DataExtType { get; set; }
        public string DataExtValue { get; set; }
    }
    public class BaseRef
    {
        [XmlElement(ElementName = "ListID")]
        public string ListID { get; set; }
        [XmlElement(ElementName = "FullName")]
        public string FullName { get; set; }
    }

}
