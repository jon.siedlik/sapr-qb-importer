﻿using CommonHelper;
using QBXMLRP2ELib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QBHelper
{
    public class QBConnector
    {
        public RequestProcessor2 RequestProcessor;
        public string Ticket;
        public string ErrorMessage;
        bool ConnectionOpen = false;
        public bool SessionBegun = false;

        public QBConnector(string connectionPath = "")
        {
            if (!CreateConnection(connectionPath))
            {
            }
        }

        private bool CreateConnection(string connectionPath)
        {
            string APPID = Constants.cOWNER_FIELDS;
            string cAppName = Constants.cAPP_NAME;
            try
            {
                if (!SessionBegun)
                {
                    RequestProcessor = new RequestProcessor2();
                    RequestProcessor.OpenConnection(APPID, cAppName);
                    ConnectionOpen = true;
                    Ticket = RequestProcessor.BeginSession(connectionPath, QBFileModeE.qbFileOpenDoNotCare);
                    SessionBegun = true;
                }
                return SessionBegun;
            }
            catch (Exception e)
            {
                Logs.Log.Add(e);
                ErrorMessage = e.Message;
                if (SessionBegun)
                    RequestProcessor.EndSession(Ticket);
                if (ConnectionOpen)
                    RequestProcessor.CloseConnection();

                return false;
            }
        }

        public void CloseConnection()
        {
            if (RequestProcessor != null)
            {
                if (SessionBegun)
                    RequestProcessor.EndSession(Ticket);
                SessionBegun = false;
                if (ConnectionOpen)
                    RequestProcessor.CloseConnection();
                ConnectionOpen = false;

            }
        }
    }
}
