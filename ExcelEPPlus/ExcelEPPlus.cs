﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelEPPlus
{
    public class ExcelEPPlus
    {
        private const int MAX_EMPTY_ROWS = 10;
        private const int MAX_EMPTY_COLUMNS = 10;

        public static DataTable GetDataTableComplex(Stream fileStream, int? sheetNumber, string sheetName, bool lowerCaseColumnsCaptions)
        {
            DataTable dt = new DataTable();

            using (ExcelPackage xlPackage = new ExcelPackage(fileStream))
            {
                ExcelWorksheet worksheet = GetWorksheet(xlPackage, sheetNumber, sheetName);

                if (worksheet == null)
                {
                    return dt;
                }

                int rowsCount = GetExcelLastRow(worksheet, MAX_EMPTY_ROWS, MAX_EMPTY_COLUMNS);
                int colsCount = GetExcelLastColumn(worksheet, MAX_EMPTY_COLUMNS, MAX_EMPTY_ROWS);

                DeleteEmptyCols(ref worksheet, colsCount, rowsCount);
                DeleteEmptyRows(ref worksheet, rowsCount, colsCount);

                int rowAfterTrimCount = GetExcelLastRow(worksheet, 0, colsCount);
                int colAfterTrimCount = GetExcelLastColumn(worksheet, 0, rowAfterTrimCount);

                dt = GetDataTableByTrimmedWorksheet(worksheet, rowAfterTrimCount, colAfterTrimCount, lowerCaseColumnsCaptions);
            }

            return dt;
        }

        private static ExcelWorksheet GetWorksheet(ExcelPackage xlPackage, int? sheetNumber, string sheetName)
        {
            OfficeOpenXml.ExcelWorksheet worksheet = null;
            if (sheetNumber.HasValue)
            {
                worksheet = xlPackage.Workbook.Worksheets[sheetNumber.Value];
            }
            else
            {
                for (int sheetIndex = 1; sheetIndex <= xlPackage.Workbook.Worksheets.Count; ++sheetIndex)
                {
                    worksheet = xlPackage.Workbook.Worksheets[sheetIndex];
                    if (worksheet.Name == sheetName)
                    {
                        break;
                    }
                }
            }

            return worksheet;
        }

        private static DataTable GetDataTableByTrimmedWorksheet(ExcelWorksheet worksheet, int rowCount, int colCount, bool lowerCaseColumnsCaptions)
        {
            DataTable dt = new DataTable();

            int colIndex = 1;

            while (colIndex <= colCount)
            {
                string val = Convert.ToString(worksheet.Cells[1, colIndex].Value);

                string columnCaption = val;
                if (string.IsNullOrEmpty(columnCaption))
                {
                    columnCaption = $"column {colIndex}";
                }
                if (lowerCaseColumnsCaptions)
                {
                    columnCaption = columnCaption.ToLower();
                }
                dt.Columns.Add(columnCaption);
                colIndex++;
            }

            int rowIndex = 2;
            colIndex = 1;

            while (rowIndex <= rowCount)
            {
                DataRow dtRow = dt.NewRow();
                colIndex = 1;

                while (colIndex <= colCount)
                {
                    string val = Convert.ToString(worksheet.Cells[rowIndex, colIndex].Value);
                    dtRow[colIndex - 1] = val;
                    colIndex++;
                }

                dt.Rows.Add(dtRow);
                rowIndex++;
            }

            return dt;
        }

        private static void DeleteEmptyRows(ref ExcelWorksheet worksheet, int lastRowInExcel, int colsInExcel)
        {
            int rowCount = lastRowInExcel;
            int rowIndex = 1;
            while (rowIndex <= rowCount)
            {
                if (!GetIsExcelRowHaveValues(worksheet, rowIndex, colsInExcel))
                {
                    worksheet.DeleteRow(rowIndex);
                    rowCount--;
                    continue;
                }

                rowIndex++;
            }
        }

        private static void DeleteEmptyCols(ref ExcelWorksheet worksheet, int lastColumnInExcel, int rowsInExcel)
        {
            int colCount = lastColumnInExcel;
            int colIndex = 1;
            while (colIndex <= colCount)
            {
                if (!GetIsExcelColumnHaveValues(worksheet, colIndex, rowsInExcel))
                {
                    worksheet.DeleteColumn(colIndex);
                    colCount--;
                    continue;
                }

                colIndex++;
            }
        }

        private static int GetExcelLastColumn(ExcelWorksheet worksheet, int maxEmptyColumns, int rowsNumberToCheck)
        {
            int colIndex = 0;
            bool isExcelColumnHaveValues = true;

            while (isExcelColumnHaveValues)
            {
                int emptyColsCounter = 0;
                colIndex++;
                isExcelColumnHaveValues = GetIsExcelColumnHaveValues(worksheet, colIndex, rowsNumberToCheck);
                while (!isExcelColumnHaveValues && emptyColsCounter < maxEmptyColumns)
                {
                    colIndex++;
                    emptyColsCounter++;
                    isExcelColumnHaveValues = GetIsExcelColumnHaveValues(worksheet, colIndex, rowsNumberToCheck);
                }
            }

            colIndex--;
            return colIndex;
        }

        private static int GetExcelLastRow(ExcelWorksheet worksheet, int maxEmptyRows, int colsNumberToCheck)
        {
            int rowIndex = 0;
            bool isExcelRowHaveValues = true;

            while (isExcelRowHaveValues)
            {
                int emptyRowsCounter = 0;
                rowIndex++;
                isExcelRowHaveValues = GetIsExcelRowHaveValues(worksheet, rowIndex, colsNumberToCheck);
                while (!isExcelRowHaveValues && emptyRowsCounter < maxEmptyRows)
                {
                    rowIndex++;
                    emptyRowsCounter++;
                    isExcelRowHaveValues = GetIsExcelRowHaveValues(worksheet, rowIndex, colsNumberToCheck);
                }
            }

            rowIndex--;
            return rowIndex;
        }

        private static bool GetIsExcelColumnHaveValues(ExcelWorksheet worksheet, int colIndex, int rowsNumberToCheck)
        {
            int rowIndex = 0;

            while (rowIndex <= rowsNumberToCheck)
            {
                rowIndex++;
                var val = Convert.ToString(worksheet.Cells[rowIndex, colIndex].Value);

                if (!string.IsNullOrEmpty(val))
                {
                    return true;
                }
            }

            return false;
        }

        private static bool GetIsExcelRowHaveValues(ExcelWorksheet worksheet, int rowIndex, int colsNumberToCheck)
        {
            int colIndex = 0;

            while (colIndex <= colsNumberToCheck)
            {
                colIndex++;
                var val = Convert.ToString(worksheet.Cells[rowIndex, colIndex].Value);

                if (!string.IsNullOrEmpty(val))
                {
                    return true;
                }
            }

            return false;
        }

        public static DataTable GetDataTable(string fileName, int? sheetNumber, string sheetName, bool lowerCaseColumnsCaptions)
        {

            DataTable dt = new DataTable();

            FileInfo file = new FileInfo(fileName);
            using (ExcelPackage xlPackage = new ExcelPackage(file))
            {
                OfficeOpenXml.ExcelWorksheet worksheet = null;
                if (sheetNumber.HasValue)
                {
                    worksheet = xlPackage.Workbook.Worksheets[sheetNumber.Value];
                }
                else
                {
                    bool found = false;
                    for (int sheetIndex = 1; sheetIndex <= xlPackage.Workbook.Worksheets.Count; ++sheetIndex)
                    {
                        worksheet = xlPackage.Workbook.Worksheets[sheetIndex];
                        if (worksheet.Name == sheetName)
                        {
                            found = true;
                            break;
                        }
                    }
                    //worksheet = xlPackage.Workbook.Worksheets[input.SheetName];
                    if (!found)
                    {
                        throw new Exception(string.Format("Missing sheet {0}", sheetName));
                    }
                }



                int x = 0;
                string val = Convert.ToString(worksheet.Cells[1, 1 + x].Value);
                while (!string.IsNullOrEmpty(val))
                {
                    string columnCaption = val;
                    if (lowerCaseColumnsCaptions)
                    {
                        columnCaption = columnCaption.ToLower();
                    }
                    dt.Columns.Add(columnCaption);
                    ++x;
                    val = Convert.ToString(worksheet.Cells[1, 1 + x].Value);
                }


                int y = 1;
                string[,] dataData = new string[dt.Columns.Count, 100];
                bool done = false;
                while (!done)
                {
                    done = true;
                    List<object> rowData = new List<object>();
                    for (x = 0; x < dt.Columns.Count; ++x)
                    {
                        var value = worksheet.Cells[1 + y, 1 + x].Value;
                        if (value is double && double.IsNaN((double)value))
                        {
                            value = null;
                        }
                        if (value is float && float.IsNaN((float)value))
                        {
                            value = null;
                        }

                        string data = Convert.ToString(value);
                        if (!string.IsNullOrEmpty(data))
                        {
                            done = false;
                        }
                        rowData.Add(data);
                    }
                    if (done)
                    {
                        continue;
                    }
                    ++y;
                    dt.Rows.Add(rowData.ToArray());
                    x = 0;
                }


            }
            return dt;

        }

        public static ReadDataTableFromExcelOutput ReadDataTableFromExcel(ReadDataTableFromExcelInput input)
        {
            ReadDataTableFromExcelOutput output = new ReadDataTableFromExcelOutput();
            try
            {
                output.Data = GetDataTable(input.FilePath, input.SheetNumber, input.SheetName, false);
                output.IsSuccess = true;

            }
            catch (Exception ex)
            {
                output.IsSuccess = false;
                output.ErrorMessage = ex.Message;
            }
            return output;
        }

        public static CreateExcelFromDataSetOutput CreateExcelFromDataSet(CreateExcelFromDataSetInput input)
        {
            CreateExcelFromDataSetOutput output = new CreateExcelFromDataSetOutput();
            if (File.Exists(input.FilePath))
            {
                File.Delete(input.FilePath);
            }
            var file = new FileInfo(input.FilePath);

            foreach (DataTable dt in input.Data.Tables)
            {
                using (var package = new ExcelPackage(file))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add(dt.TableName);

                    int x = 1;
                    int y = 1;
                    foreach (DataColumn column in dt.Columns)
                    {
                        worksheet.Cells[y, x].Value = column.Caption;
                        ++x;
                    }
                    ++y;

                    foreach (DataRow row in dt.Rows)
                    {
                        x = 1;
                        foreach (DataColumn column in dt.Columns)
                        {
                            worksheet.Cells[y, x].Value = row[x - 1];
                            if (column.DataType == typeof(DateTime))
                            {
                                worksheet.Cells[y, x].Style.Numberformat.Format = "dd/MM/yyyy hh:mm";
                            }
                            if (column.DataType == typeof(double))
                            {
                                worksheet.Cells[y, x].Style.Numberformat.Format = "#.#";
                            }


                            ++x;
                        }
                        ++y;
                    }

                    x = 1;
                    foreach (DataColumn column in dt.Columns)
                    {
                        worksheet.Column(x).AutoFit();
                        ++x;
                    }

                    package.Save();

                }
            }
            output.IsSuccess = true;
            return output;
        }

        public static CreateExcelFromDataTableOutput CreateExcelFromDataTable(CreateExcelFromDataTableInput input)
        {
            CreateExcelFromDataTableOutput output = new CreateExcelFromDataTableOutput();
            if (File.Exists(input.FilePath))
            {
                File.Delete(input.FilePath);
            }
            var file = new FileInfo(input.FilePath);


            using (var package = new ExcelPackage(file))
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add(input.SheetName);

                int x = 1;
                int y = 1;
                foreach (DataColumn column in input.Data.Columns)
                {
                    worksheet.Cells[y, x].Value = column.Caption;
                    ++x;
                }
                ++y;

                foreach (DataRow row in input.Data.Rows)
                {
                    x = 1;
                    foreach (DataColumn column in input.Data.Columns)
                    {
                        worksheet.Cells[y, x].Value = row[x - 1];
                        if (column.DataType == typeof(DateTime))
                        {
                            worksheet.Cells[y, x].Style.Numberformat.Format = "dd/MM/yyyy hh:mm";
                        }
                        if (column.DataType == typeof(double))
                        {
                            worksheet.Cells[y, x].Style.Numberformat.Format = "#.#";
                        }


                        ++x;
                    }
                    ++y;
                }

                x = 1;
                foreach (DataColumn column in input.Data.Columns)
                {
                    worksheet.Column(x).AutoFit();
                    ++x;
                }

                package.Save();
            }
            output.IsSuccess = true;
            return output;
        }

        public static CreateExcelFromCellsOutput CreateExcelFromCells(CreateExcelFromCellsInput input)
        {
            CreateExcelFromCellsOutput output = new CreateExcelFromCellsOutput();
            if (File.Exists(input.FilePath))
            {
                File.Delete(input.FilePath);
            }
            var file = new FileInfo(input.FilePath);


            using (var package = new ExcelPackage(file))
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add(input.SheetName);
                foreach (CreateExcelFromCellInput cell in input.Cells)
                {
                    if (cell.Value.GetType() == typeof(DateTime))
                    {
                        worksheet.Cells[cell.Y1Based, cell.X1Based].Style.Numberformat.Format = input.DateFormat;
                    }
                    if (cell.Value.GetType() == typeof(double))
                    {
                        worksheet.Cells[cell.Y1Based, cell.X1Based].Style.Numberformat.Format = "#.#";
                    }

                    worksheet.Cells[cell.Y1Based, cell.X1Based].Value = cell.Value;
                }


                for (int x = 1; x < input.Cells.Max(z => z.X1Based); ++x)
                {
                    worksheet.Column(x).AutoFit();
                }


                package.Save();
            }
            output.IsSuccess = true;
            return output;
        }

    }


    public class ReadDataTableFromExcelInput
    {
        public string FilePath;
        public string SheetName;
        public int? SheetNumber;
    }

    public class ReadDataTableFromExcelOutput
    {
        public DataTable Data;
        public bool IsSuccess;
        public string ErrorMessage;
    }


    public class CreateExcelFromDataSetInput
    {
        public string FilePath;
        public string SheetName;
        public DataSet Data;
    }

    public class CreateExcelFromDataSetOutput
    {
        public bool IsSuccess;
        public string ErrorMessage;
    }

    public class CreateExcelFromDataTableInput
    {
        public string FilePath;
        public string SheetName;
        public DataTable Data;
    }

    public class CreateExcelFromDataTableOutput
    {
        public bool IsSuccess;
        public string ErrorMessage;
    }

    public class CreateExcelFromCellsInput
    {
        public string FilePath;
        public string SheetName;
        public string DateFormat; // "dd/MM/yyyy hh:mm"
        public List<CreateExcelFromCellInput> Cells = new List<CreateExcelFromCellInput>();
    }

    public class CreateExcelFromCellInput
    {
        public object Value;
        public int X1Based;
        public int Y1Based;
    }


    public class CreateExcelFromCellsOutput
    {
        public bool IsSuccess;
        public string ErrorMessage;
    }
}
