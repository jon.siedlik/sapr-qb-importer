﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Logs
{
	public class Log
	{
		private const string separator = "<---------------------------------{0}------------------------------->";

		protected const string ctMessageHeader = "MESSAGE";

		protected const string ctErrorHeader = "ERROR";

		protected static string appName = "";

		protected static string extraMessageLine = "";

		public static bool logAssemblyVersion = false;

		public static int MaxLogSize
		{
			get
			{
				return FileSplitter.MaxLogSize;
			}
			set
			{
				FileSplitter.MaxLogSize = value;
			}
		}

		public static string ApplicationName
		{
			get
			{
				return Log.appName;
			}
			set
			{
				Log.appName = value;
			}
		}

		public static string ExtraLineMessage
		{
			get
			{
				return Log.extraMessageLine;
			}
			set
			{
				Log.extraMessageLine = value;
			}
		}

		public static bool LogAssemblyVersion
		{
			get
			{
				return Log.logAssemblyVersion;
			}
			set
			{
				Log.logAssemblyVersion = value;
			}
		}

		protected static void Add(string message, string header, bool showMessage)
		{
			StreamWriter streamWriter = null;
			new Process();
			try
			{
				string logFileName = FileSplitter.GetLogFileName();
				if (File.Exists(logFileName))
				{
					streamWriter = new StreamWriter(logFileName, true, Encoding.UTF8, 512);
				}
				else
				{
					streamWriter = new StreamWriter(logFileName);
				}
				string value = string.Format("<---------------------------------{0}------------------------------->", header);
				streamWriter.WriteLine(value);
				if (Log.LogAssemblyVersion)
				{
					Type callingType = Log.GetCallingType();
					if (callingType != null)
					{
						try
						{
							Assembly assembly = Assembly.GetAssembly(callingType);
							streamWriter.WriteLine(string.Concat(new string[]
							{
								Log.appName,
								" : ",
								assembly.GetName().Name,
								" version ",
								assembly.GetName().Version.ToString()
							}));
						}
						catch
						{
						}
					}
				}
				if (Log.extraMessageLine != null && Log.extraMessageLine.Trim() != "")
				{
					streamWriter.WriteLine(Log.extraMessageLine);
				}
				streamWriter.WriteLine(DateTime.Now.ToString());
				streamWriter.WriteLine(message);
				streamWriter.WriteLine(value);
				streamWriter.WriteLine("\n");
				Trace.WriteLine(header + ":\n" + message);
				streamWriter.Close();
				streamWriter = null;
			}
			catch (Exception ex)
			{
				MessageBox.Show("Logging exception :" + ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			finally
			{
				if (streamWriter != null)
				{
					streamWriter.Close();
				}
			}
		}

		protected static string GetCallingInformation()
		{
			StackTrace stackTrace = new StackTrace(true);
			string result;
			try
			{
				StackFrame frame = stackTrace.GetFrame(2);
				string name = frame.GetMethod().DeclaringType.Name;
				string name2 = frame.GetMethod().Name;
				result = name + " " + name2;
			}
			catch
			{
				result = "";
			}
			return result;
		}

		protected static Type GetCallingType()
		{
			StackTrace stackTrace = new StackTrace(true);
			Type result;
			try
			{
				for (int i = 0; i < stackTrace.FrameCount; i++)
				{
					StackFrame frame = stackTrace.GetFrame(i);
					if (!frame.GetMethod().DeclaringType.Module.Assembly.Equals(Assembly.GetExecutingAssembly()))
					{
						result = frame.GetMethod().DeclaringType;
						return result;
					}
				}
				result = null;
			}
			catch
			{
				result = null;
			}
			return result;
		}

		public static void Add(string message, bool showMessage)
		{
			Log.Add(message, "MESSAGE", showMessage);
		}

		public static void Add(Exception ex, bool showMessage)
		{
			Log.Add(ex.Message, "ERROR", showMessage);
			Log.Add(ex.StackTrace);
		}

		public static void Add(Exception ex, string message, bool showMessage)
		{
			Log.Add(ex, false);
			Log.Add(message, showMessage);
		}

		public static void Add(string message)
		{
			Log.Add(message, false);
		}

		public static void Add(Exception ex)
		{
			Log.Add(ex, false);
		}

		public static void Add(Exception ex, string message)
		{
			Log.Add(ex, message, false);
		}
	}
}
