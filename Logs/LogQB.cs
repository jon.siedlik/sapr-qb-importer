﻿using QBFC13Lib;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Logs
{
	public class LogQB : Log
	{
		private const string ctQBErrorHeader = "QUICK BOOKS RESPONSE ERROR";

		private static object caller = null;

		private static string methodName = "ErrorHandler";

		private static string error = "";

		public static string SetErrorHandlerName
		{
			set
			{
				LogQB.methodName = value;
			}
		}

		public static object Caller
		{
			set
			{
				LogQB.caller = value;
			}
		}

		protected static bool ParseStatusMessage(IResponse resp)
		{
			if (resp.StatusMessage.ToLower().IndexOf("exceeded") > -1 && resp.StatusSeverity.ToUpper() != "OK")
			{
				try
				{
					LogQB.error = resp.StatusMessage.Replace("QuickBooks error message: ", "\n").Replace("&quot", "\"");
				}
				catch
				{
				}
				LogQB.RaiseErrorEvent();
				return true;
			}
			return false;
		}

		protected static void RaiseErrorEvent()
		{
			StackTrace stackTrace = new StackTrace(true);
			try
			{
				MethodInfo methodInfo = null;
				object[] array = new object[1];
				for (int i = 0; i < stackTrace.FrameCount; i++)
				{
					StackFrame frame = stackTrace.GetFrame(i);
					array[0] = LogQB.error;
					try
					{
						methodInfo = frame.GetMethod().DeclaringType.GetMethod(LogQB.methodName);
					}
					catch
					{
					}
					if (methodInfo != null)
					{
						i = stackTrace.FrameCount;
						break;
					}
				}
				if (methodInfo != null && LogQB.caller != null)
				{
					methodInfo.Invoke(LogQB.caller, array);
				}
			}
			catch (Exception ex)
			{
				Trace.WriteLine(ex.Message);
			}
		}

		public static void Add(string info, IMsgSetRequest req, IMsgSetResponse resp, bool showMessage)
		{
			bool showMessage2 = false;
			try
			{
				if (resp != null && resp.ResponseList != null && req.RequestList != null)
				{
					bool flag = false;
					for (int i = 0; i < resp.ResponseList.Count; i++)
					{
						IResponse at = resp.ResponseList.GetAt(i);
						string text;
						if (flag)
						{
							string format = "Response log:\n{0}, Severity:\n{1}";
							text = string.Format(format, at.StatusMessage, at.StatusSeverity);
						}
						else
						{
							string format = "Request log:\n{0} Response log:\n{1}, Severity:\n{2}";
							text = string.Format(format, req.ToXMLString(), at.StatusMessage, at.StatusSeverity);
						}
						if (at.StatusCode != 0)
						{
							if (!flag && showMessage)
							{
								showMessage2 = false;
							}
							else if (flag && showMessage)
							{
								showMessage2 = true;
							}
							flag = true;
							if (info == null || info == "")
							{
								Log.Add(text, "QUICK BOOKS RESPONSE ERROR", showMessage2);
							}
							else
							{
								Log.Add(info + ":" + text, "QUICK BOOKS RESPONSE ERROR", showMessage2);
							}
						}
						if (LogQB.ParseStatusMessage(at))
						{
							break;
						}
					}
				}
			}
			catch
			{
			}
		}

		public static void Add(string info, IMsgSetRequest req, IMsgSetResponse resp)
		{
			LogQB.Add(info, req, resp, false);
		}
	}
}
