﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Logs
{
	public class FileSplitter
	{
		public const string extension = ".log";

		public const string logName = "ErrorLog";

		public static int logCount = 1;

		public static int fileSize = 524536;

		public static int MaxLogSize
		{
			get
			{
				return FileSplitter.fileSize;
			}
			set
			{
				FileSplitter.fileSize = value;
			}
		}

		public static string GetLogFileName()
		{
			try
			{
				DirectoryInfo directoryInfo = new DirectoryInfo(FileSplitter.LogDirName());
				if (!directoryInfo.Exists)
				{
					directoryInfo.Create();
				}
				FileInfo[] files = directoryInfo.GetFiles("*.log");
				string result;
				if (files.Length == 0)
				{
					result = FileSplitter.LogDirName() + "\\ErrorLog" + FileSplitter.logCount.ToString() + ".log";
					return result;
				}
				for (int i = 0; i < files.Length; i++)
				{
					string fullName = files[i].FullName;
					int num = 0;
					try
					{
						num = int.Parse(fullName.ToLower().Replace(FileSplitter.LogDirName().ToLower() + "\\ErrorLog", "").Replace(".log", ""));
					}
					catch
					{
					}
					if (num >= FileSplitter.logCount)
					{
						FileSplitter.logCount = num;
					}
				}
				FileInfo fileInfo = new FileInfo(FileSplitter.LogDirName() + "\\ErrorLog" + FileSplitter.logCount.ToString() + ".log");
				if (fileInfo.Length >= (long)FileSplitter.fileSize)
				{
					FileSplitter.logCount++;
				}
				result = FileSplitter.LogDirName() + "\\ErrorLog" + FileSplitter.logCount.ToString() + ".log";
				return result;
			}
			catch (Exception)
			{
			}
			return FileSplitter.LogDirName() + "\\ErrorLog" + FileSplitter.logCount.ToString() + ".log";
		}

		protected static string LogDirName()
		{
			return $"{GetCommonBaseDirectory()}\\{DateTime.Now.ToString("yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture)}";
		}

		public static string GetCommonBaseDirectory()
        {
			var versionInfo = FileVersionInfo.GetVersionInfo(Assembly.GetEntryAssembly().Location);
			string baseDir = $"{Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData)}\\{versionInfo.CompanyName}";
            if (!Directory.Exists(baseDir))
            {
				
				Directory.CreateDirectory(baseDir);
            }
			return baseDir;

		}

		internal static FileInfo[] GetErrorLoggersList()
		{
			DirectoryInfo directoryInfo = new DirectoryInfo(FileSplitter.LogDirName());
			if (!directoryInfo.Exists)
			{
				directoryInfo.Create();
			}
			return directoryInfo.GetFiles("*.log");
		}
	}
}
