﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logs
{
	public class ErrorLoggerSender
	{
		private static string errorServiceURL = "";

		private static string password = "";

		public static string ErrorReportingServiceURL
		{
			get
			{
				return ErrorLoggerSender.errorServiceURL;
			}
			set
			{
				ErrorLoggerSender.errorServiceURL = value;
			}
		}

		public static string WebServicePassword
		{
			get
			{
				return ErrorLoggerSender.password;
			}
			set
			{
				ErrorLoggerSender.password = value;
			}
		}

		public static bool Send(string errorLoggerFileName)
		{
			bool result;
			try
			{
				StreamReader streamReader = new StreamReader(errorLoggerFileName);
				streamReader.ReadToEnd();
				streamReader.Close();
				result = true;
			}
			catch (Exception ex)
			{
				Log.Add(ex);
				result = false;
			}
			return result;
		}
	}
}
