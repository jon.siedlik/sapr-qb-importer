﻿using CommonHelper;
using CommonHelper.Enums;
using CommonHelper.Models;
using Logs;
using QBHelper;
using QBHelper.ModelAdd;
using QBHelper.ModelRet;
using QBHelper.QB;
using QBHelper.Ref;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessImporter.Service
{
    public class TimeTrackingService
    {
        private QBTimeTracking _qbTimeTraking;
        private QBEmployee _qBEmployee;
        List<CustomerRet> _listCustomer;
        List<EmployeeRet> _listEmployes;
        string _connectionPath;
        public TimeTrackingService(string connectionPath)
        {
            _connectionPath = connectionPath;
            _qbTimeTraking = new QBTimeTracking(_connectionPath);
            _qBEmployee = new QBEmployee(_connectionPath);
            _listEmployes = _qBEmployee.GetAllEmployee().ToList();
            _listCustomer = new QBCustomer(_connectionPath).GetAllSyncCustomers().ToList();
        }


        public GlobalValue.Reponse SaveTimeTracking(TimeTracking timeTracking)
        {
            GlobalValue.Reponse responses = new GlobalValue.Reponse();
            try
            {
                TimeTrackingAdd timeTrackAdd = new TimeTrackingAdd();
				EmployeeRet employee = null;
                if (!string.IsNullOrEmpty(timeTracking.PayrollID))
                {
                    employee = _listEmployes.FirstOrDefault(a => a.DataExtRet.Any(b => b.DataExtName == "PayrollID" && b.DataExtValue == timeTracking.PayrollID) == true);
                }
                else if (!string.IsNullOrEmpty(timeTracking.AccountNumber))
                {
                    employee = _listEmployes.FirstOrDefault(a => a.AccountNumber == timeTracking.AccountNumber);
                }
				
                if (employee == null)
                {
                    responses.IsError = true;
                    responses.Message = "Employee does not exist!";
                    return responses;
                }

                timeTrackAdd.EntityRef = new EntityRef
                {
                    ListID = employee.ListID
                };


                if (!string.IsNullOrEmpty(timeTracking.ClassName))
                {
                    timeTrackAdd.ClassRef = new ClassRef { FullName = timeTracking.ClassName };
                }

                timeTrackAdd.TxnDate = DateHelper.ToDateString(timeTracking.TimeTrackingDate);
                timeTrackAdd.Notes = timeTracking.Notes;
                var customer = _listCustomer.FirstOrDefault(a => a.AdditionalContactRef.Any(b=>b.ContactValue == timeTracking.Customer && b.ContactName == "Other 1"));
                if (customer == null)
                {
                    responses.IsError = true;
                    responses.Message = "Customer does not exist!";
                    return responses;
                }

                if (!string.IsNullOrEmpty(timeTracking.Customer))
                {
                    timeTrackAdd.CustomerRef = new CustomerRef { ListID = customer.ListID };
                }
                if (!string.IsNullOrEmpty(timeTracking.PayrollItemName))
                {
                    timeTrackAdd.PayrollItemWageRef = new PayrollItemWageRef { FullName = "Hourly Wages" };
                }
                //if (!string.IsNullOrEmpty(timeTracking.ServiceItemName))
                //{
                //    timeTrackAdd.ItemServiceRef = new ItemServiceRef { FullName = timeTracking.ServiceItemName };
                //}
                if(timeTracking.Duration == "0")
                {
                    responses.IsError = true;
                    responses.Message = "Log hours is 0";
                    return responses;
                }
                timeTrackAdd.Duration = Common.GetDuration(Convert.ToDecimal(timeTracking.Duration));
                timeTrackAdd.BillableStatus = "NotBillable";
                //timeTrackAdd.IsBillable = true; //timeTracking.IsBillable;

                 List<GlobalValue.Reponse> saveBill = _qbTimeTraking.SaveTimeTracking(new List<TimeTrackingAdd> { timeTrackAdd });
                responses = saveBill[0];
            }
            catch (Exception ex)
            {
                Log.Add(ex);
                responses.IsError = true;
                responses.Message = ex.Message;
            }
            return responses;


        }

        public List<GlobalValue.Reponse> SaveTimeTracking(List<TimeTracking> timeTrackings)
        {
            List<GlobalValue.Reponse> responses = new List<GlobalValue.Reponse>();
            foreach (var timeTracking in timeTrackings)
            {
                GlobalValue.Reponse reponse = new GlobalValue.Reponse();
                try
                {
                    TimeTrackingAdd timeTrackAdd = new TimeTrackingAdd();
                    timeTrackAdd.EntityRef = new EntityRef
                    {
                        ListID = timeTracking.Name
                    };
                    if (!string.IsNullOrEmpty(timeTracking.ClassName))
                    {
                        timeTrackAdd.ClassRef = new ClassRef { FullName = timeTracking.ClassName };
                    }
                    timeTrackAdd.TxnDate = DateHelper.ToDateString(timeTracking.TimeTrackingDate);
                    timeTrackAdd.Notes = timeTracking.Notes;
                    if (!string.IsNullOrEmpty(timeTracking.Customer))
                    {
                        timeTrackAdd.CustomerRef = new CustomerRef { FullName = timeTracking.Customer };
                    }
                    if (!string.IsNullOrEmpty(timeTracking.PayrollItemName))
                    {
                        timeTrackAdd.PayrollItemWageRef = new PayrollItemWageRef { FullName = timeTracking.PayrollItemName };
                    }
                    if (!string.IsNullOrEmpty(timeTracking.ServiceItemName))
                    {
                        timeTrackAdd.ItemServiceRef = new ItemServiceRef { FullName = timeTracking.ServiceItemName };
                    }

                    timeTrackAdd.Duration = timeTracking.Duration;
                    timeTrackAdd.BillableStatus = timeTracking.BilableStatus;
                    timeTrackAdd.IsBillable = timeTracking.IsBillable;

                    responses = _qbTimeTraking.SaveTimeTracking(new List<TimeTrackingAdd> { timeTrackAdd });

                }
                catch (Exception ex)
                {
                    Log.Add(ex);
                    reponse.IsError = true;
                    reponse.Message = ex.Message;
                }
            }
            return responses;
        }

        public List<EmployeeRet> GetAllEmployee(DateTime? fromDate = null)
        {
            List<EmployeeRet> employeeAdds = _qBEmployee.GetAllEmployee();
            return employeeAdds;
        }

        public List<TimeTrackingRet> getAllTimeTracks()
        {
            List<TimeTrackingRet> reponses = _qbTimeTraking.getAllTimeTracks();
            return reponses;
        }
    }
}
