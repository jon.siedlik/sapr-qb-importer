﻿using CommonHelper;
using CommonHelper.Models;
using Logs;
using QBHelper.ModelAdd;
using QBHelper.ModelRet;
using QBHelper.QB;
using QBHelper.Ref;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessImporter.Service
{
    public class CustomerService
    {
        private QBCustomer _qbCustomer;
        List<CustomerRet> _listCustomer;
        string _connectionPath;
        public CustomerService(string connectionPath)
        {
            _connectionPath = connectionPath;
            _qbCustomer = new QBCustomer(connectionPath);
            _listCustomer = _qbCustomer.GetAllSyncCustomers().ToList();
        }


        public GlobalValue.Reponse SaveCustomerJob(CustomerJob customerJob)
        {
            GlobalValue.Reponse responses = new GlobalValue.Reponse();
            try
            {
                //_qbCustomer = new QBCustomer(_connectionPath);

                CustomerAdd customerAdd = new CustomerAdd();
                var customer = _listCustomer.FirstOrDefault(a => a.AdditionalContactRef.Any(b => b.ContactValue == customerJob.ParentRef && b.ContactName == "Other 1"));
                if (customer == null)
                {
                    responses.IsError = true;
                    responses.Message = "Customer does not exist!";
                    return responses;
                }
                else
                {
                    customerAdd.ParentRef = new ParentRef
                    {
                        ListID = customer.ListID
                    };
                }
                customerAdd.Name = customerJob.Name;

                List<GlobalValue.Reponse> saveBill = new QBCustomer(_connectionPath).SaveCustomerJobs(new List<CustomerAdd> { customerAdd });
                responses = saveBill[0];
            }
            catch (Exception ex)
            {
                Log.Add(ex);
                responses.IsError = true;
                responses.Message = ex.Message;
            }
            return responses;
        }

        public List<GlobalValue.Reponse> SaveCustomerJob(List<CustomerJob> customerJobs)
        {
            List<GlobalValue.Reponse> responses = new List<GlobalValue.Reponse>();
            _qbCustomer = new QBCustomer(_connectionPath);
            foreach (var customerJob in customerJobs)
            {
                GlobalValue.Reponse reponse = new GlobalValue.Reponse();
                try
                {
                    CustomerAdd customerAdd = new CustomerAdd();
                    var customer = _listCustomer.FirstOrDefault(a => a.AdditionalContactRef.Any(b => b.ContactValue == customerJob.ParentRef && b.ContactName == "Other 1"));
                    if (customer == null)
                    {
                        reponse.IsError = true;
                        reponse.Message = "Customer does not exist!";
                        return responses;
                    }
                    else
                    {
                        customerAdd.ParentRef = new ParentRef
                        {
                            ListID = customer.ListID
                        };
                    }
                    customerAdd.Name = customerJob.Name;
                    responses = _qbCustomer.SaveCustomerJobs(new List<CustomerAdd> { customerAdd });

                }
                catch (Exception ex)
                {
                    Log.Add(ex);
                    reponse.IsError = true;
                    reponse.Message = ex.Message;
                }
            }
            return responses;
        }
    }
}
