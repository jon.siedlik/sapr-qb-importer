﻿using BusinessImporter.Service.Models;
using Logs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace BusinessImporter.Service
{
    public static class SettingsService
    {
        public static string XmlFilePath { get; set; }
        public static Settings Settings { get; set; }
        public static void Initialize()
        {

            if (string.IsNullOrWhiteSpace(XmlFilePath))
            {
                throw new MissingMemberException("Xml file path not provided!");
            }

            FileExistOrCreate();

            try
            {
                XDocument.Load(XmlFilePath);
                XmlSerializer serializer = new XmlSerializer(typeof(Settings));
                XmlTextReader reader = new XmlTextReader(XmlFilePath);
                Settings = (Settings)serializer.Deserialize(reader);
                reader.Close();
            }
            catch (Exception)
            {
                Settings = new Settings();
            }
        }

        public static void Initialize(string path)
        {
            XmlFilePath = path;
            Initialize();
        }

        public static void Save()
        {
            try
            {
                FileExistOrCreate();
                XmlSerializer xsSubmit = new XmlSerializer(typeof(Settings));
                string xmlContent = "";
                using (StringWriter sww = new StringWriter())
                {
                    using (XmlWriter writer = XmlWriter.Create(sww))
                    {
                        xsSubmit.Serialize(writer, Settings);
                        xmlContent = sww.ToString();
                    }
                }
                XmlDocument expr_80 = new XmlDocument();
                expr_80.LoadXml(xmlContent);
                expr_80.Save(XmlFilePath);
            }
            catch (Exception arg_8F_0)
            {
                Log.Add(arg_8F_0);
            }
        }

        private static void FileExistOrCreate()
        {
            if (!File.Exists(XmlFilePath))
            {
                File.Create(XmlFilePath).Close();
            }
        }

    }
}
