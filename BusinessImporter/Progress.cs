﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BusinessImporter
{
    public partial class Progress : Form
    {
        [DllImport("user32.dll")]
        private static extern bool SetForegroundWindow(int handle);

        [DllImport("Gdi32.dll")]
        private static extern IntPtr CreateRoundRectRgn(int nLeftRect, int nTopRect, int nRightRect, int nBottomRect, int nWidthEllipse, int nHeightEllipse);

        public Progress(string message = null)
        {
            InitializeComponent();
            if(!string.IsNullOrEmpty(message))
            {
                txtText.Text = message;
            }
            LoadSetting();
        }

        private void LoadSetting()
        {
            this.FormBorderStyle = FormBorderStyle.None;
            this.Region = Region.FromHrgn(Progress.CreateRoundRectRgn(0, 0, base.Width, base.Height, 7, 7));
        }

        private void Progress_Load(object sender, EventArgs e)
        {
            Progress.SetForegroundWindow(base.Handle.ToInt32());
            this.WindowState = FormWindowState.Minimized;
            this.WindowState = FormWindowState.Normal;
            this.Activate();
        }

        public void ExitDialog()
        {
            try
            {
                this.DialogResult = DialogResult.OK;
            }
            catch(Exception ex)
            {
                Logs.Log.Add(ex.Message);
            }
        }
    }
}
