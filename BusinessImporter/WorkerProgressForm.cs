﻿using CommonHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BusinessImporter
{
    public partial class WorkerProgressForm : Form
    {
        private readonly BackgroundWorker _worker;
        public WorkerProgressForm(BackgroundWorker worker, string progressTitle = null)
        {
            InitializeComponent();
            progressBar.Step = 1;
            progressBar.Minimum = 0;
            progressBar.Maximum = 100;
            this.Text = progressTitle;
            _worker = worker;
        }

        #region Public Methods

        public void SetPosition(int progressPercentage)
        {
            if (InvokeRequired)
            {
                Invoke((MethodInvoker)delegate
                {
                    SetPosition(progressPercentage);
                });
                return;
            }
            if (!Visible) return;

            progressBar.Value = progressPercentage;

            if (!btnStop.Enabled && btnStop.Visible)
            {
                btnStop.Enabled = true;
            }
        }

        public void SetMessage(string progressMessage,bool showMessageBox = false)
        {
            if (InvokeRequired)
            {
                Invoke((MethodInvoker)delegate
                {
                    SetMessage(progressMessage);
                });
                return;
            }
            if (!Visible) return;

            //progressLabel.Text = progressMessage;

            if (!btnStop.Enabled && btnStop.Visible)
            {
                btnStop.Enabled = true;
            }
        }

        public void SetMessage(GlobalValue.Reponse response)
        {
            if (InvokeRequired)
            {
                Invoke((MethodInvoker)delegate
                {
                    SetMessage(response);
                });
                return;
            }
            if (!Visible) return;

            importLogDataRow.Rows.Add(response.IsError ? "Error" : "success", response.Message);

            if (!btnStop.Enabled && btnStop.Visible)
            {
                btnStop.Enabled = true;
            }
        }

        #endregion //Public Methods

        #region Private Methods

        private void OnWorkerProgressFormLoaded(object sender, EventArgs e)
        {
            Invoke((MethodInvoker)delegate
            {
                if (_worker.WorkerSupportsCancellation && _worker.IsBusy)
                {
                    btnStop.Text = "Stop";
                    btnStop.Visible = true;
                }
            });
        }

        private void OnCancelWorkerButtonClicked(object sender, EventArgs e)
        {
            if (_worker.IsBusy)
            {
                btnStop.Text = "Close";
                _worker.CancelAsync();
            }

            if(btnStop.Text == "Close")
            {
                this.Close();
            }
        }
        #endregion //Private Methods

        public void UpdateButtonText(string text)
        {
            btnStop.Text = text;
        }
    }
}
