﻿using BusinessImporter.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BusinessImporter
{
    public partial class SettingForm : Form
    {
        public SettingForm()
        {
            InitializeComponent();
            LoadSetting();
        }

        private void LoadSetting()
        {
            txtLocation.Text = SettingsService.Settings.QBCompanyFilePath;
        }
        private void btnLocation_Click(object sender, EventArgs e)
        {
            OpenFileDialog of = new OpenFileDialog();
            of.Filter = "Quickbook |*.qbw";
            of.FilterIndex = 1;
            of.Multiselect = false;
            of.ValidateNames = false;
            if (of.ShowDialog() == DialogResult.OK)
            {
                this.txtLocation.Text = of.FileName;
                SettingsService.Settings.QBCompanyFilePath = of.FileName;
                SettingsService.Save();
                DialogResult dialogResult = MessageBox.Show("You have successfully selected company file. Do you want to close setting window?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialogResult == DialogResult.Yes)
                {
                    this.Close();
                }
            }

        }
    }
}
