﻿using CommonHelper;
using Logs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace BusinessImporter
{
    public class SettingsService
    {
        public Settings Settings { get; set; }

        string xml = Common.GetCommonBaseDirectory() + "\\GeneralSetting.xml";
        
        public SettingsService()
        {
            Settings = new Settings();
        }

        public void Reload()
        {
            try
            {
                if (!File.Exists(xml))
                {
                    File.Create(xml).Close();
                    this.Settings = new Settings();
                }
                XDocument.Load(xml);
                XmlSerializer serializer = new XmlSerializer(typeof(Settings));
                XmlTextReader reader = new XmlTextReader(xml);
                this.Settings = (Settings)serializer.Deserialize(reader);
                reader.Close();
            }
            catch (Exception)
            {
                this.Settings = new Settings();
            }
        }

        public void Save()
        {
            try
            {
                if (!File.Exists(xml))
                {
                    File.Create(xml).Close();
                }
                XmlSerializer xsSubmit = new XmlSerializer(typeof(Settings));
                string xmlContent = "";
                using (StringWriter sww = new StringWriter())
                {
                    using (XmlWriter writer = XmlWriter.Create(sww))
                    {
                        xsSubmit.Serialize(writer, this.Settings);
                        xmlContent = sww.ToString();
                    }
                }
                XmlDocument expr_80 = new XmlDocument();
                expr_80.LoadXml(xmlContent);
                expr_80.Save(xml);
            }
            catch (Exception arg_8F_0)
            {
                Log.Add(arg_8F_0);
            }
        }

    }
}
