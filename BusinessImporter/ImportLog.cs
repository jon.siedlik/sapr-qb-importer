﻿using BusinessImporter.Helper;
using BusinessImporter.Service;
using CommonHelper;
using CommonHelper.Enums;
using CommonHelper.Models;
using QBHelper.ModelRet;
using QBHelper.QB;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BusinessImporter
{
    public partial class ImportLog : Form
    {
        List<TimeTracking> _listTimeTracking;
        List<CustomerJob> _listCustomerJob;
        private CallBackThread logCallBackThread;
        TransactionType _transactionType;
        int _totalRecord;
        public ImportLog(DataTable importData,TransactionType transactionType)
        {
            InitializeComponent();
            _transactionType = transactionType;
            switch ((TransactionType)transactionType)
            {
                case TransactionType.TimeTracking:
                    _listTimeTracking = Common.DataTableToList<TimeTracking>(importData);
                    _totalRecord = _listTimeTracking?.Count ?? 0;
                    break;
                case TransactionType.Job:
                    _listCustomerJob = Common.DataTableToList<CustomerJob>(importData);
                    _totalRecord = _listCustomerJob?.Count ?? 0;

                    break;
                default:
                    break;
            }
            
            Control argcaller = this;
            ThreadFunctionDelegate argthreadMethod = ProcessData;
            CallBackDelegate argcallbackFunction = CallBackMethod1;
            logCallBackThread = new CallBackThread(ref argcaller, ref argthreadMethod, ref argcallbackFunction);
            progressBar1.Minimum = 0;
            progressBar1.Maximum = 100;
            logCallBackThread.Start();
            
            this.ShowDialog();
        }

        private void ProcessData()
        {
            switch (_transactionType)
            {
                case TransactionType.TimeTracking:
                    TimeTrackingService timeTrackingService = new TimeTrackingService(SettingsService.Settings.QBCompanyFilePath);
                    for (int i = 0; i < _listTimeTracking.Count; i++)
                    {
                        var result = timeTrackingService.SaveTimeTracking(_listTimeTracking[i]);
                        logCallBackThread.UpdateUI(result, i + 1);
                    }
                    break;
                case TransactionType.Job:
                    CustomerService customerService = new CustomerService(SettingsService.Settings.QBCompanyFilePath);
                    for (int i = 0; i < _listCustomerJob.Count; i++)
                    {
                        var result = customerService.SaveCustomerJob(_listCustomerJob[i]);
                        logCallBackThread.UpdateUI(result, i + 1);
                    }
                    break;
                default:
                    break;
            }
           
        }

        private void CallBackMethod1(GlobalValue.Reponse response, int recordNum)
        {
            progressBar1.Value = (recordNum * 100) / _totalRecord;
            importLogDataRow.Rows.Add(response.IsError ? "Error" : "success", response.Message);

            if(recordNum == _totalRecord)
            {
                logCallBackThread.Dispose();
                progressBar1.Value = 100;
                btnStop.Text = "Close";
            }
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            if (btnStop.Text == "Close")
            {
                this.Close();
            }

            if (logCallBackThread is object)
            {
                logCallBackThread.Dispose();
                btnStop.Text = "Close";
            }
        }
    }
}
