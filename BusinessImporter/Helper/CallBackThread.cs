﻿using CommonHelper;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BusinessImporter.Helper
{
    public delegate void CallBackDelegate(GlobalValue.Reponse reponse, int recorNum);

    /// <summary>
    /// The parameterless Thread function delegate
    /// </summary>
    /// <remarks></remarks>
    public delegate void ThreadFunctionDelegate();

    public class CallBackThread : IDisposable
    {
        private Control m_BaseControl;                    // The Form/Control that implements the call back function
        private ThreadFunctionDelegate m_ThreadFunction;  // The pointer to the function that implements the thread logic
        private CallBackDelegate m_CallBackFunction;      // The pointer to the call back function
        private System.Threading.Thread m_Thread;                // internal thread object
        private bool m_disposedValue = false;          // internal falg to detect redundant calls
        private bool m_startedValue = false;           // internal falg to detect redundant calls

        /// <summary>
        /// Instanciates the Call back thread
        /// </summary>
        /// <param name="caller">The Form/Control that implements the call back function</param>
        /// <param name="threadMethod">The pointer to the function that implements the thread logic(use AddressOf() to assign)</param>
        /// <param name="callbackFunction">The pointer to the call back function(use AddressOf() to assign)</param>
        /// <remarks>Call 'Start' to start thread and use UpdateUI to send status messages to UI</remarks>
        public CallBackThread(ref Control caller, ref ThreadFunctionDelegate threadMethod, ref CallBackDelegate callbackFunction)

        {
            m_BaseControl = caller;
            m_ThreadFunction = threadMethod;
            m_CallBackFunction = callbackFunction;
            m_Thread = new System.Threading.Thread(ThreadFunction);
        }

        /// <summary>
        /// Starts the internal thread
        /// </summary>
        /// <remarks></remarks>
        public void Start()
        {
            if (!m_startedValue)
            {
                m_Thread.Start();
                m_startedValue = true;
            }
            else
            {
                throw new Exception("Thread already started");
            }
        }

        private void ThreadFunction()
        {
            m_ThreadFunction.Invoke();
            m_startedValue = false;
        }

        /// <summary>
        /// Sends the status to the Form/Control that implements the Call back method.
        /// </summary>
        /// <param name="msg">the message to send</param>
        /// <remarks></remarks>
        public void UpdateUI(GlobalValue.Reponse reponse, int recordNum)
        {
            if (m_BaseControl is object && m_CallBackFunction is object)
            {
                m_BaseControl.Invoke(m_CallBackFunction, new object[] { reponse, recordNum });
            }
        }

        // IDisposable
        protected virtual void Dispose(bool disposing)
        {
            if (!m_disposedValue)
            {
                if (disposing)
                {
                    // TODO: free unmanaged resources when explicitly called
                    if (m_Thread.ThreadState != System.Threading.ThreadState.Stopped)
                    {
                        m_Thread.Abort();
                    }
                }

                m_Thread = null;
                m_BaseControl = null;
                m_CallBackFunction = null;
            }

            m_disposedValue = true;
        }

        /* TODO ERROR: Skipped RegionDirectiveTrivia */    // This code added by Visual Basic to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
