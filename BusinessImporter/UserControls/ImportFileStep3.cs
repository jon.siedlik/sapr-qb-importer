﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Aspose.Cells;
using CommonHelper.Models;

namespace BusinessImporter.UserControls
{
    public partial class ImportFileStep3 : UserControl
    {
        public DataTable importDataTable { get; private set; }
        public ImportFileStep3()
        {
            InitializeComponent();
        }

        public void LoadControl(List<ImportColumnMapping> importColumnMappings, DataTable dataTable)
        {
            List<string> selectedColumns = new List<string>();
            foreach (var item in importColumnMappings)
            {
                if (item.UserFieldName != null)
                {
                    dataTable.Columns[item.UserFieldName].ColumnName = $"{item.QuickbookFieldName}";
                    selectedColumns.Add($"{ item.QuickbookFieldName}");
                }
            }


            DataTable dt = new DataView(dataTable).ToTable(false, selectedColumns.ToArray());
            importDataTable = dt;
            dataGridViewPreview.DataSource = dt;
        }
    }
}
