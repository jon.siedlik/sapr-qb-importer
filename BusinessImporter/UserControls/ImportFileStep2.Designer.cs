﻿namespace BusinessImporter.UserControls
{
    partial class ImportFileStep2
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImportFileStep2));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnSaveMapping = new System.Windows.Forms.Button();
            this.btnClearMapping = new System.Windows.Forms.Button();
            this.btnOpenMapping = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.mappingDataGridView = new System.Windows.Forms.DataGridView();
            this.colQuickbookFields = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colFileColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.colDefaultValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mappingDataGridView)).BeginInit();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(270, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(166, 24);
            this.label1.TabIndex = 5;
            this.label1.Text = "Step 2 : Mapping";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(748, 47);
            this.panel1.TabIndex = 6;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 47);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(748, 94);
            this.panel2.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 26);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(226, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "1) Quickbook fields are listed in the left column";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(442, 39);
            this.label4.TabIndex = 2;
            this.label4.Text = resources.GetString("label4.Text");
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(330, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "At this step you are to match QuickBooks fields to data from your file.";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.tableLayoutPanel1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 454);
            this.panel3.Margin = new System.Windows.Forms.Padding(5);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(748, 54);
            this.panel3.TabIndex = 8;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Controls.Add(this.btnSaveMapping, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnClearMapping, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnOpenMapping, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(748, 54);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // btnSaveMapping
            // 
            this.btnSaveMapping.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnSaveMapping.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSaveMapping.Location = new System.Drawing.Point(569, 14);
            this.btnSaveMapping.Name = "btnSaveMapping";
            this.btnSaveMapping.Size = new System.Drawing.Size(107, 25);
            this.btnSaveMapping.TabIndex = 2;
            this.btnSaveMapping.Text = "Save Mapping";
            this.btnSaveMapping.UseVisualStyleBackColor = true;
            this.btnSaveMapping.Click += new System.EventHandler(this.btnSaveMapping_Click);
            // 
            // btnClearMapping
            // 
            this.btnClearMapping.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnClearMapping.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnClearMapping.Location = new System.Drawing.Point(76, 14);
            this.btnClearMapping.Name = "btnClearMapping";
            this.btnClearMapping.Size = new System.Drawing.Size(97, 25);
            this.btnClearMapping.TabIndex = 0;
            this.btnClearMapping.Text = "Clear Mapping";
            this.btnClearMapping.UseVisualStyleBackColor = true;
            this.btnClearMapping.Click += new System.EventHandler(this.btnClearMapping_Click);
            // 
            // btnOpenMapping
            // 
            this.btnOpenMapping.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnOpenMapping.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnOpenMapping.Location = new System.Drawing.Point(321, 14);
            this.btnOpenMapping.Name = "btnOpenMapping";
            this.btnOpenMapping.Size = new System.Drawing.Size(105, 25);
            this.btnOpenMapping.TabIndex = 1;
            this.btnOpenMapping.Text = "Open Mapping";
            this.btnOpenMapping.UseVisualStyleBackColor = true;
            this.btnOpenMapping.Click += new System.EventHandler(this.btnOpenMapping_Click);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.mappingDataGridView);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 141);
            this.panel4.Name = "panel4";
            this.panel4.Padding = new System.Windows.Forms.Padding(3);
            this.panel4.Size = new System.Drawing.Size(748, 313);
            this.panel4.TabIndex = 9;
            // 
            // mappingDataGridView
            // 
            this.mappingDataGridView.AllowUserToAddRows = false;
            this.mappingDataGridView.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            this.mappingDataGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.mappingDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.mappingDataGridView.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.mappingDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.mappingDataGridView.ColumnHeadersHeight = 25;
            this.mappingDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colQuickbookFields,
            this.colFileColumn,
            this.colDefaultValue});
            this.mappingDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mappingDataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.mappingDataGridView.Location = new System.Drawing.Point(3, 3);
            this.mappingDataGridView.Name = "mappingDataGridView";
            this.mappingDataGridView.RowHeadersVisible = false;
            this.mappingDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.mappingDataGridView.Size = new System.Drawing.Size(742, 307);
            this.mappingDataGridView.TabIndex = 0;
            this.mappingDataGridView.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.mappingDataGridView_DataError);
            this.mappingDataGridView.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.mappingDataGridView_EditingControlShowing);
            // 
            // colQuickbookFields
            // 
            this.colQuickbookFields.DataPropertyName = "QuickbookFieldName";
            this.colQuickbookFields.HeaderText = "Quickbooks Fields";
            this.colQuickbookFields.Name = "colQuickbookFields";
            // 
            // colFileColumn
            // 
            this.colFileColumn.DataPropertyName = "UserFieldName";
            this.colFileColumn.HeaderText = "File Columns";
            this.colFileColumn.Name = "colFileColumn";
            // 
            // colDefaultValue
            // 
            this.colDefaultValue.DataPropertyName = "DefaultValue";
            this.colDefaultValue.HeaderText = "Default Value";
            this.colDefaultValue.Name = "colDefaultValue";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.panel4);
            this.panel5.Controls.Add(this.panel3);
            this.panel5.Controls.Add(this.panel2);
            this.panel5.Controls.Add(this.panel1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(748, 508);
            this.panel5.TabIndex = 4;
            // 
            // ImportFileStep2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel5);
            this.Name = "ImportFileStep2";
            this.Size = new System.Drawing.Size(748, 508);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mappingDataGridView)).EndInit();
            this.panel5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnSaveMapping;
        private System.Windows.Forms.Button btnOpenMapping;
        private System.Windows.Forms.Button btnClearMapping;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.DataGridView mappingDataGridView;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colQuickbookFields;
        private System.Windows.Forms.DataGridViewComboBoxColumn colFileColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDefaultValue;
    }
}
