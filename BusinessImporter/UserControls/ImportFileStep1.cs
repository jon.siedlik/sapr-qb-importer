﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BusinessImporter.Helper;
using System.Net;
using CommonHelper.Enums;
using CommonHelper.Models;
using System.IO;
using CommonHelper;
using BusinessImporter.Service;

namespace BusinessImporter.UserControls
{
    public partial class ImportFileStep1 : UserControl
    {
        public string SelectedFile { get; private set; }
        public string SelectedSheet { get; private set; }
        public int SelectedTransactionType { get; private set; }
        public ImportFileStep1()
        {
            InitializeComponent();
        }

        public void LoadSetting()
        {
            LoadDropDown();
            cmbQuickbookTransactionListType.SelectedIndex = 0;
            if (string.IsNullOrEmpty(SettingsService.Settings.QBCompanyFilePath))
            {
                MessageBox.Show("Please select QB company file first!");
                return; 
            }
            if (!string.IsNullOrEmpty(SettingsService.Settings.ExcelFilePath) && File.Exists(SettingsService.Settings.ExcelFilePath))
            {
                SelectedFile = SettingsService.Settings.ExcelFilePath;
                txtSelectedFile.Text = SelectedFile;
                LoadSheetDropDown(SelectedFile);
            }
        }

        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog of = new OpenFileDialog();
            of.Filter = "CSV files (*.csv)|*.csv|Excel Files|*.xls;*.xlsx";
            of.FilterIndex = 1;
            of.Multiselect = false;
            if (of.ShowDialog() == DialogResult.OK)
            {
                txtSelectedFile.Text = of.FileName;
                SelectedFile = of.FileName;
                LoadSheetDropDown(of.FileName);
                SettingsService.Settings.ExcelFilePath = of.FileName;
                SettingsService.Save();
            }
        }

        private void LoadSheetDropDown(string fileName)
        {
            cmbSheetSelection.Items.Clear();
            ExcelReader excelReader = new ExcelReader(fileName);
            cmbSheetSelection.Items.AddRange(excelReader.GetListOfSheet().ToArray());

            if (cmbSheetSelection.Items.Count > 0)
            {
                cmbSheetSelection.SelectedIndex = 0;
                SelectedSheet = cmbSheetSelection.SelectedItem.ToString();
            }
        }

        private void cmbSheetSelection_SelectedValueChanged(object sender, EventArgs e)
        {
            SelectedSheet = cmbSheetSelection.SelectedItem.ToString();
        }

        private void LoadDropDown()
        {
            List<EnumDropDown> enumDropDowns = TransactionType.TimeTracking.ToSelectList().ToList();

            cmbQuickbookTransactionListType.DataSource = enumDropDowns;
            cmbQuickbookTransactionListType.DisplayMember = "Text";
            cmbQuickbookTransactionListType.ValueMember = "Value";
        }

        private void cmbQuickbookTransactionListType_SelectedValueChanged(object sender, EventArgs e)
        {
            EnumDropDown transactionType = (EnumDropDown)cmbQuickbookTransactionListType.SelectedItem;
            if (transactionType != null)
            {
                SelectedTransactionType = Convert.ToInt32(transactionType.Value);
            }
        }
    }
}
