﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BusinessImporter.Helper;
using CommonHelper.Models;
using CommonHelper.Enums;
using System.Net;
using System.Threading;
using Logs;
using System.IO;
using CommonHelper;
using System.Xml;
using System.Windows.Threading;

namespace BusinessImporter.UserControls
{
    public partial class ImportFileStep2 : UserControl
    {
        private string _selectedSheetName;
        private Dispatcher dispatcher;
        private string _filePath = string.Empty;
        private ExcelReader _excelReader;
        private static Progress progress = new Progress();
        public List<ImportColumnMapping> ImportColumnMappings { get; private set; }
        public DataTable ExcelData { get; private set; }
        int _transactionType;
        public ImportFileStep2()
        {
            InitializeComponent();
            dispatcher = Dispatcher.CurrentDispatcher;
        }

        public void ShowProgress()
        {
            try
            {
                dispatcher.BeginInvoke(new ThreadStart(() =>
                {
                    progress.ShowDialog();
                }));
            }
            catch
            {
            }
        }

        public void HideProgress()
        {
            try
            {
                dispatcher.BeginInvoke(new ThreadStart(() =>
                {
                    progress.Close();
                }));
            }
            catch
            {
            }
        }

        public void LoadData(string sheetName, string filePath, int transactionType)
        {
            try
            {
                progress.txtText.Text = "Loading excel data for mapping ..";
                ShowProgress();
                //new Thread(new ThreadStart(frmMain.ShowProgress)).Start();
                _selectedSheetName = sheetName;
                _filePath = filePath;
                _excelReader = new ExcelReader(_filePath);
                _transactionType = transactionType;
                LoadGrid();
                HideProgress();
            }
            catch (Exception ex)
            {
                Log.Add(ex);
                progress.ExitDialog();
            }
        }

        private void LoadGrid()
        {
            mappingDataGridView.AutoGenerateColumns = false;
            mappingDataGridView.DataSource = null;

            var excelData = _excelReader.GetDataFromExcelFile(_selectedSheetName);
            ExcelData = excelData;
            List<string> userColumns = _excelReader.GetUserColumns(excelData);

            colFileColumn.Items.AddRange(userColumns.ToArray());

            var importFields = _excelReader.GetImportFromExcelFields(_transactionType);
            var columnMappings = _excelReader.GetColumnMappings(excelData, importFields, _transactionType);

            mappingDataGridView.DataSource = columnMappings;

        }

        public void LoadMapping()
        {
            ImportColumnMappings = mappingDataGridView.DataSource as List<ImportColumnMapping>;
        }

        private void mappingDataGridView_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is DataGridViewComboBoxEditingControl)
            {
                ((ComboBox)e.Control).DropDownStyle = ComboBoxStyle.DropDown;
                ((ComboBox)e.Control).AutoCompleteSource = AutoCompleteSource.ListItems;
                ((ComboBox)e.Control).AutoCompleteMode = AutoCompleteMode.Suggest;
            }

            if (mappingDataGridView.CurrentCell.ColumnIndex == 1 && e.Control is ComboBox)
            {
                ComboBox comboBox = e.Control as ComboBox;
                comboBox.SelectedIndexChanged -= LastColumnComboSelectionChanged;
                comboBox.SelectedIndexChanged += LastColumnComboSelectionChanged;
            }
        }

        private void LastColumnComboSelectionChanged(object sender, EventArgs e)
        {
            bool result = true;
            var currentcell = mappingDataGridView.CurrentCellAddress;
            var sendingCB = sender as DataGridViewComboBoxEditingControl;
            string comboValue = sendingCB.EditingControlFormattedValue.ToString();//EditingControlFormattedValue
            if (!string.IsNullOrEmpty(comboValue))
            {
                for (int i = 0; i < mappingDataGridView.Rows.Count; i++)
                {
                    if (currentcell.Y != i)
                    {
                        string classValue = mappingDataGridView.Rows[i].Cells[1].EditedFormattedValue.ToString();
                        if (!string.IsNullOrEmpty(classValue) && classValue == comboValue)
                        {
                            MessageBox.Show("Already selected for different field!");
                            result = false;
                            break;
                        }
                    }
                }

                if (!result)
                {
                    sendingCB.EditingControlFormattedValue = "";
                }
            }

        }

        private void btnClearMapping_Click(object sender, EventArgs e)
        {
            LoadData(_selectedSheetName, _filePath, _transactionType);
        }

        private void btnOpenMapping_Click(object sender, EventArgs e)
        {

        }

        private void btnSaveMapping_Click(object sender, EventArgs e)
        {
            LoadMapping();
            SaveTSMapping();
        }

        public bool SaveTSMapping()
        {
            string mappingPath = Path.Combine(Common.GetCommonBaseDirectory(), Constants.MAPPING_DIRECTORY);
            if (!Directory.Exists(mappingPath))
                Directory.CreateDirectory(mappingPath);
            if (ImportColumnMappings?.Count > 0)
            {
                if (Validate(ImportColumnMappings, (TransactionType)_transactionType))
                {
                    string filePath = string.Empty;
                    XmlWriterSettings setting;
                    switch ((TransactionType)_transactionType)
                    {
                        case TransactionType.TimeTracking:
                            filePath = $"{mappingPath}\\{Constants.TIMESHEET_MAPFILE}";
                            setting = new XmlWriterSettings();
                            setting.ConformanceLevel = ConformanceLevel.Auto;
                            using (XmlWriter writer = XmlWriter.Create(filePath, setting))
                            {
                                writer.WriteStartElement("TimeSheets");
                                foreach (var cust in ImportColumnMappings)
                                {
                                    writer.WriteStartElement("TimeSheet");
                                    writer.WriteElementString("QuickbookFieldName", cust.QuickbookFieldName.ToString());
                                    writer.WriteElementString("UserFieldName", cust.UserFieldName);
                                    writer.WriteElementString("DefaultValue", cust.DefaultValue);
                                    writer.WriteEndElement();
                                }
                                writer.WriteEndElement();
                                writer.Flush();
                            }
                            break;
                        case TransactionType.Job:
                            filePath = $"{mappingPath}\\{Constants.CUSTOMERJOB_MAPFILE}";
                             setting = new XmlWriterSettings();
                            setting.ConformanceLevel = ConformanceLevel.Auto;
                            using (XmlWriter writer = XmlWriter.Create(filePath, setting))
                            {
                                writer.WriteStartElement("CustomerJobs");
                                foreach (var cust in ImportColumnMappings)
                                {
                                    writer.WriteStartElement("CustomerJob");
                                    writer.WriteElementString("QuickbookFieldName", cust.QuickbookFieldName.ToString());
                                    writer.WriteElementString("UserFieldName", cust.UserFieldName);
                                    writer.WriteElementString("DefaultValue", cust.DefaultValue);
                                    writer.WriteEndElement();
                                }
                                writer.WriteEndElement();
                                writer.Flush();
                            }
                            break;
                        default:
                            break;
                    }

                }
            }

            return false;
        }

        public bool Validate(List<ImportColumnMapping> importColumnMappings, TransactionType transactionType)
        {
            if (importColumnMappings != null)
            {
                switch (transactionType)
                {
                    case TransactionType.TimeTracking:
                        foreach (string s in Constants.RequiredFieldForTimeTracking)
                        {
                            if (importColumnMappings.FirstOrDefault(t => t.QuickbookFieldName.ToLowerInvariant() == s.ToLowerInvariant()).UserFieldName == null)
                            {
                                MessageBox.Show($"This fields {string.Join(",", Constants.RequiredFieldForTimeTracking)}  are required please mapped!");
                                return false;
                            }
                        }
                        return true;
                    case TransactionType.Job:
                        foreach (string s in Constants.RequiredFieldForJob)
                        {
                            if (importColumnMappings.FirstOrDefault(t => t.QuickbookFieldName.ToLowerInvariant() == s.ToLowerInvariant()).UserFieldName == null)
                            {
                                MessageBox.Show($"This fields {string.Join(",", Constants.RequiredFieldForJob)}  are required please mapped!");
                                return false;
                            }
                        }
                        return true;
                    default:
                        break;
                }


                return false;
            }
            else
            {
                return false;
            }
        }

        public Dictionary<string, string> ReadMappingFile(string filename, string tableName)
        {
            DataSet ds = new DataSet();
            string mappingPath = Path.Combine(Constants.PATH_LOACATION, Constants.MAPPING_DIRECTORY);
            Dictionary<string, string> mapping = new Dictionary<string, string>();

            if (File.Exists(mappingPath))
            {
                ds.ReadXml(mappingPath);
                if (ds.Tables.Contains(tableName))
                {
                    foreach (DataRow dr in ds.Tables[tableName].Rows)
                    {
                        mapping[dr[0].ToString()] = dr[1].ToString();
                    }
                }
            }

            return mapping;
        }

        private void mappingDataGridView_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }
    }
}
