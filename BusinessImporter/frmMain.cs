﻿using BusinessImporter.Service;
using BusinessImporter.UserControls;
using CommonHelper;
using CommonHelper.Enums;
using CommonHelper.Models;
using QBHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Threading;

namespace BusinessImporter
{
    public partial class frmMain : Form
    {
        int panelWidth;
        bool Hidden;
        private int step = 1;
        private static Progress progress = new Progress();
        private string qbPath = string.Empty;
        QBConnector qBConnector;
        WorkerProgressForm workerProgressForm;
        public frmMain()
        {
            InitializeComponent();
            LoadSetting();
            panelWidth = panelSlide.Width;
            Hidden = false;
        }

        private void LoadSetting()
        {
            if (string.IsNullOrEmpty(SettingsService.Settings.QBCompanyFilePath))
            {
                SettingForm settingForm = new SettingForm();
                settingForm.ShowDialog();
            }

            qbPath = SettingsService.Settings.QBCompanyFilePath;
            importFileStep.LoadSetting();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            Traverse(step);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            try
            {
                progress.txtText.Text = "Connecting to Quickbook!";
                ShowProgress();
                qBConnector = new QBConnector(qbPath);
                if (qBConnector.Ticket == null && !qBConnector.SessionBegun)
                {
                    HideProgress();
                    MessageBox.Show(qBConnector.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    qBConnector = null;
                    return;
                }
                qBConnector.CloseConnection();
                HideProgress();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                HideProgress();
                return;
            }

            if (string.IsNullOrEmpty(importFileStep.SelectedFile))
            {
                MessageBox.Show("Please select File first!");
                return;
            }

            if (string.IsNullOrEmpty(importFileStep.SelectedSheet))
            {
                MessageBox.Show("Please select Sheet from drop down!");
                return;
            }

            Traverse(++step);
        }

        private void btnPrev_Click(object sender, EventArgs e)
        {
            Traverse(--step);
        }

        private void Traverse(int step)
        {
            lblSteps.Text = string.Format("Completed {0} of 3 Steps", step - 1);
            switch (step)
            {
                case 1:
                    importFileStep.Visible = true;
                    importFileStep2.Visible = false;
                    importFileStep3.Visible = false;
                    btnStartImport.Visible = false;
                    btnNext.Visible = btnNext.Enabled = true;
                    btnPrev.Enabled = false;
                    break;
                case 2:
                    importFileStep.Visible = false;
                    importFileStep2.Visible = true;
                    importFileStep3.Visible = false;
                    btnStartImport.Visible = false;
                    btnNext.Visible = btnNext.Enabled = btnPrev.Enabled = true;
                    importFileStep2.LoadData(importFileStep.SelectedSheet, importFileStep.SelectedFile, importFileStep.SelectedTransactionType);
                    break;
                case 3:
                    importFileStep2.LoadMapping();
                    if (importFileStep2.Validate(importFileStep2.ImportColumnMappings, (TransactionType)importFileStep.SelectedTransactionType))
                    {
                        importFileStep2.SaveTSMapping();
                        importFileStep.Visible = false;
                        importFileStep2.Visible = false;
                        importFileStep3.Visible = true;
                        btnStartImport.Visible = btnPrev.Enabled = true;
                        btnNext.Visible = btnNext.Enabled = false;
                    }
                    else
                    {
                        this.step = 2;
                        return;
                    }
                    importFileStep3.LoadControl(importFileStep2.ImportColumnMappings, importFileStep2.ExcelData);
                    break;
                default:
                    break;
            }
        }

        private bool runningExclusiveProcess = false;

        private void btnStartImport_Click(object sender, EventArgs e)
        {
            if (importFileStep3.importDataTable?.Rows.Count > 0)
            {
                new ImportLog(importFileStep3.importDataTable,(TransactionType)importFileStep.SelectedTransactionType);
            }
            else
            {
                MessageBox.Show("No data found please upload excel again!");
                return;
            }
        }

        private void SyncTimeSheetData(object sender, DoWorkEventArgs e)
        {
            Trace.WriteLine("Starting ...", "SyncTimeSheetDoWork");
            var worker = (BackgroundWorker)sender;
            List<TimeTracking> _listTimeTracking = Common.DataTableToList<TimeTracking>(importFileStep3.importDataTable);
            TimeTrackingService timeTrackingService = new TimeTrackingService(SettingsService.Settings.QBCompanyFilePath);

            if (_listTimeTracking?.Count == 0)
            {
                Trace.WriteLine("EXIT Syncronization. Data not Found !!!");
                return;
            }

            int total = _listTimeTracking.Count;
            int syncedItems = 0;
            workerProgressForm.SetMessage("Start Uploading!");
            for (int i = 0; i < _listTimeTracking.Count; i++)
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                    return;
                }

                var result = timeTrackingService.SaveTimeTracking(_listTimeTracking[i]);

                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                    return;
                }

                workerProgressForm.SetMessage(result);
                syncedItems++;
                if (total != 0)
                {
                    worker.ReportProgress((syncedItems * 100) / total);
                }
            }
            workerProgressForm.SetMessage("Uploading Success successfully!", true); ;
        }

        private void SyncJobData(object sender, DoWorkEventArgs e)
        {
            Trace.WriteLine("Starting ...", "SyncJobData");
            var worker = (BackgroundWorker)sender;
            List<CustomerJob> _listCustomerjob = Common.DataTableToList<CustomerJob>(importFileStep3.importDataTable);
            CustomerService customerService = new CustomerService(SettingsService.Settings.QBCompanyFilePath);

            if (_listCustomerjob?.Count == 0)
            {
                Trace.WriteLine("EXIT Syncronization. Data not Found !!!");
                return;
            }

            int total = _listCustomerjob.Count;
            int syncedItems = 0;
            workerProgressForm.SetMessage("Start Uploading!");
            for (int i = 0; i < _listCustomerjob.Count; i++)
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                    return;
                }

                var result = customerService.SaveCustomerJob(_listCustomerjob[i]);

                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                    return;
                }

                workerProgressForm.SetMessage(result);
                syncedItems++;
                if (total != 0)
                {
                    worker.ReportProgress((syncedItems * 100) / total);
                }
            }
            workerProgressForm.SetMessage("Uploading Success successfully!", true); ;
        }

        private Dispatcher dispatcher;
        public void ShowProgress()
        {
            try
            {
                dispatcher = Dispatcher.CurrentDispatcher;
                dispatcher.BeginInvoke(new ThreadStart(() =>
                {
                    progress.ShowDialog();
                }));
            }
            catch
            {
            }
        }

        public void HideProgress()
        {
            try
            {
                dispatcher.BeginInvoke(new ThreadStart(() =>
                {
                    progress.Close();
                }));
            }
            catch
            {
            }
        }

        private void menuOpen_Click(object sender, EventArgs e)
        {
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (Hidden)
            {
                panelSlide.Width = panelSlide.Width + 10;
                if (panelSlide.Width >= panelWidth)
                {
                    timer1.Stop();
                    Hidden = false;
                    this.Refresh();
                }
            }
            else
            {
                panelSlide.Width = panelSlide.Width - 10;
                if (panelSlide.Width <= 0)
                {
                    timer1.Stop();
                    Hidden = true;
                    this.Refresh();
                }
            }
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SettingForm settingForm = new SettingForm();
            settingForm.ShowDialog();
            LoadSetting();
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (qBConnector != null && qBConnector.SessionBegun)
                qBConnector.CloseConnection();
        }
    }
}
